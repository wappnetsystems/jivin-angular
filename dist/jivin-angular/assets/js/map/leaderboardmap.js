mapData = {};


var locationMapleaderboard = {
    cWidth: 0,
    cHeight: 0,
    move: {
        left: 0,
        top: 0
    },
    selectedData: {
        region: {
            name: '',
            id: ''
        },
        division: {
            name: '',
            id: ''
        },
        state: {
            name: '',
            id: '',
            code: ''
        }
    },

    original: { width: 814, height: 430 },
    init: function (md) {
        mapData = md;
        //console.log("RUNB",mapData);

        setTimeout(function () {
            locationMapleaderboard.createPoly("all");
            locationMapleaderboard.resize();
            locationMapleaderboard.giveRegionOption();

        }, 5000);


        $('.reset').click(function () {
            $('#location-map > img.full-map').removeClass('deactivate');
            $('#location-map > img').removeAttr('style').removeClass('on');
            locationMapleaderboard.giveRegionOption();
            setTimeout(function () { locationMapleaderboard.resize(); }, 1000);
            locationMapleaderboard.zoom($('#location-map > .map-overlay'), 1);
            $('#location-map > .map-overlay').removeAttr('style');
            locationMapleaderboard.createPoly("all");
            locationMapleaderboard.resize();
            locationMapleaderboard.selectedData.region = { name: "", id: "" } //collect and store data
            locationMapleaderboard.selectedData.division = { name: "", id: "" }
            locationMapleaderboard.selectedData.state = { name: "", id: "", code: "" }
        });
        $('.reset').trigger('click');
    },

    createPoly: function (type) {

        $('#us_mapping').html('');
        //$('#us_mapping').append('<input name="test" type="hidden" />');
        switch (type) {
            case "all":
                
                $.each(mapData.US.regions, function (r, reg) {
                   
                    var area = $(document.createElement('area')).attr({
                        shape: "poly",
                        coords: reg.poly,
                        "data-coords": reg.poly,
                        alt: r.replace(/_/g, " "),
                        title: r.replace(/_/g, " "),
                        'data-toggle': "tooltip", 'data-placement': "top"
                    }).click(function () {
                        locationMapleaderboard.selectedData = {
                            region: {
                                name: r,
                                id: reg.id
                            }

                        };
                        locationMapleaderboard.updateSelection();
                    });
                    //alert();
                    //console.log(area);

                    $('#us_mapping').append(area);

                    $.each(reg.divisions, function (d, division) {
                        $.each(division.states, function (s, state) {
                            /*var area = $(document.createElement('area')).attr({
                                shape: "poly",
                                coords: state.poly,
                                "data-coords": state.poly,
                                alt: s.replace(/_/g," "),
                                title: s.replace(/_/g," "),
                            }).click(function() { 
                                locationMapleaderboard.selectedData = {
                                    region: {
                                        name: r,
                                        id: reg.id
                                    },
                                    division: {
                                        name: d.replace(/_/g," "),
                                        code:d,
                                        id: division.id
                                    },
                                    state: {
                                        name: s.replace(/_/g," "),
                                        id: state.id,
                                        code: state.code
                                    }
                                };
                                locationMapleaderboard.updateSelection();
                            });
                            //alert();
                            //console.log(area);
                            
                            $('#us_mapping').append(area);*/

                            //$('#my-map-us').css("padding","0px");
                        });
                    });
                });
                break;
            case "region":

                $.each(mapData.US.regions[locationMapleaderboard.selectedData.region.name].divisions, function (d, division) {
                    var area = $(document.createElement('area')).attr({
                        shape: "poly",
                        coords: division.poly,
                        "data-coords": division.poly,
                        alt: d.replace(/_/g, " "),
                        title: d.replace(/_/g, " "),
                    }).click(function () {
                        locationMapleaderboard.selectedData.division = { name: d.replace(/_/g, " "), id: division.id, code: d };
                        //locationMapleaderboard.selectedData.state = {name:s.replace(/_/g," "),id:state.id,code:state.code};

                        locationMapleaderboard.updateSelection();
                    });
                    $('#us_mapping').append(area);
                    $.each(division.states, function (s, state) {
                        // var area = $(document.createElement('area')).attr({
                        //     shape:"poly",
                        //     coords:state.poly,
                        //     "data-coords":state.poly,
                        //     alt:s.replace(/_/g," "),
                        //     title:s.replace(/_/g," "),
                        // }).click(function(){
                        //     locationMapleaderboard.selectedData.division = {name:d.replace(/_/g," "),id:division.id,code:d};
                        //     locationMapleaderboard.selectedData.state = {name:s.replace(/_/g," "),id:state.id,code:state.code};

                        //     locationMapleaderboard.updateSelection();
                        // });
                        // $('#us_mapping').append(area);
                    });
                });

                break;
            case "division":

                $.each(mapData.US.regions[locationMapleaderboard.selectedData.region.name]['divisions'][locationMapleaderboard.selectedData.division.code].states, function (s, state) {
                    var area = $(document.createElement('area')).attr({
                        shape: "poly",
                        coords: state.poly,
                        "data-coords": state.poly,
                        alt: s.replace(/_/g, " "),
                        title: s.replace(/_/g, " "),
                    }).click(function () {
                        locationMapleaderboard.selectedData.state = { name: s.replace(/_/g, " "), id: state.id, code: state.code };

                        locationMapleaderboard.updateSelection();
                    });
                    $('#us_mapping').append(area);
                });

                break;

        }
        locationMapleaderboard.resize();
    },

    giveRegionOption: function () {

        $('.selection a').not('.reset').remove();
        var ra = $(document.createElement('a')).addClass('btn btn-info').html('Select Region:');
        $('#location-map .selection').append(ra);
        $.each(mapData.US.regions, function (r, reg) {

            var ra = $(document.createElement('a')).addClass('btn btn-info').html(r);
            $('#location-map .selection').append(ra);
            ra.click(function () {
                locationMapleaderboard.selectedData.region = { name: r, id: reg.id } //collect and store data
                locationMapleaderboard.selectedData.division = { name: "", id: "", code: "" } //collect and store data
                locationMapleaderboard.selectedData.state = { name: "", id: "", code: "" } //collect and store data

                $('.selection a').not('.reset').remove();
                var ra = $(document.createElement('a')).addClass('btn btn-primary region').html(r);
                $('#location-map .selection').append(ra);
                ra.click(function () {
                    $('.selection a').not('.reset').not('.region').remove();

                    locationMapleaderboard.giveDivisionOption(r)
                    locationMapleaderboard.focusOn('region', r);
                    locationMapleaderboard.selectedData.division = { name: "", id: "", code: "" } //collect and store data
                    locationMapleaderboard.selectedData.state = { name: "", id: "", code: "" }
                });
                locationMapleaderboard.giveDivisionOption(r)
                locationMapleaderboard.focusOn('region', r);
            });
        });
    },
    giveDivisionOption: function (r) {

        var s = $(document.createElement('a')).addClass('btn btn-info').html('Select division:');
        $('#location-map .selection').append(s);

        $.each(mapData.US.regions[r].divisions, function (d, division) {
            //console.log(d,division);
            var da = $(document.createElement('a')).addClass('btn btn-info divi-opt').html(d.replace(/_/g, " "));
            $('#location-map .selection').append(da);
            locationMapleaderboard.selectedData.division = { name: "", id: "", code: "" }
            locationMapleaderboard.selectedData.state = { name: "", id: "", code: "" }

            da.click(function () {

                locationMapleaderboard.selectedData.division = { name: d.replace(/_/g, " "), id: division.id, code: d } //collect and store data
                locationMapleaderboard.selectedData.state = { name: "", id: "", code: "" }

                $('.selection a').not('.reset').not('.region').remove();

                var ra = $(document.createElement('a')).addClass('btn btn-primary division').html(d.replace(/_/g, " "));
                $('#location-map .selection').append(ra);

                var s = $(document.createElement('a')).addClass('btn btn-info').html('Please Click on a State');
                $('#location-map .selection').append(s);
                locationMapleaderboard.focusOn('division', d);
            });

        })

    },
    updateSelection: function () {
        //alert(locationMapleaderboard.selectedData.region.name+'/'+locationMapleaderboard.selectedData.division.name+'/'+locationMapleaderboard.selectedData.state.name);
        //alert(locationMapleaderboard.selectedData.region.name);

        
        switch ($('#area_type_selected').val()) {
            
            case 'Country':
                //if country then load region
                var region_name = locationMapleaderboard.selectedData.region.name;
                $('#main_map_hidden').attr('src', 'assets/img/map/region/' + region_name + '.png');
                $('#main_map').attr('src', 'assets/img/map/region/' + region_name + '.png');
                $('#area_type_selected').val('Region');
                $('#selected_location_id').val(locationMapleaderboard.selectedData.region.id);
                $('#selected_location_slug').val(locationMapleaderboard.selectedData.region.name);
                $('#selected_region_name').val(region_name);
                locationMapleaderboard.createPoly('region');
                locationMapleaderboard.resize();
                $('#back_btn').show();
                break;

            case 'Region':
                //if region then load division
                var division_name = locationMapleaderboard.selectedData.division.name.replace(/ /g, "-");
                $('#main_map_hidden').attr('src', 'assets/img/map/division/' + division_name.toUpperCase() + '.png');
                $('#main_map').attr('src', 'assets/img/map/division/' + division_name.toUpperCase() + '.png');
                $('#area_type_selected').val('Devision');
                $('#selected_location_id').val(locationMapleaderboard.selectedData.division.id);
                $('#selected_division_name').val(division_name);
                $('#selected_location_slug').val(locationMapleaderboard.selectedData.division.name);
                locationMapleaderboard.createPoly('division');
                locationMapleaderboard.resize();
                break;

            case 'Devision':
                //if division then load state
                var state_name = locationMapleaderboard.selectedData.state.code;
                $('#main_map_hidden').attr('src', 'assets/img/map/state/' + state_name + '.png');
                $('#main_map').attr('src', 'assets/img/map/state/' + state_name + '.png');
                $('#area_type_selected').val('State');
                $('#selected_location_slug').val(locationMapleaderboard.selectedData.state.code);
                $('#selected_location_id').val(locationMapleaderboard.selectedData.state.id);
                $('#selected_division_name').val(state_name);

                $('#us_mapping').html('');

                break;

            default:
            $('#us_mapping').html('');
                //$('#main_map_hidden').attr('src', 'assets/img/map/full-map-new.png');
                //$('#main_map').attr('src', 'assets/img/map/full-map-new.png');
                //$('#area_type_selected').val('Country');
                break;
        }
        $('#area_type_selected').trigger('input');
        $('#selected_location_id').trigger('change');
        
        /*$('.selection a').not('.reset').remove();

        var a = $(document.createElement('a')).addClass('btn btn-primary region').html(locationMapleaderboard.selectedData.region.name);
        $('#location-map .selection').append(a);
        a.click(function(){console.log(111);
            $('.selection a').not('.reset').not('.region').remove();
            locationMapleaderboard.giveDivisionOption(locationMapleaderboard.selectedData.region.name);
            locationMapleaderboard.focusOn('region', locationMapleaderboard.selectedData.region.name);
        });

        var a = $(document.createElement('a')).addClass('btn btn-primary division').html(locationMapleaderboard.selectedData.division.name);
        $('#location-map .selection').append(a);

        var a = $(document.createElement('a')).addClass('btn btn-primary state').html(locationMapleaderboard.selectedData.state.name);
        $('#location-map .selection').append(a);*/
    },
    back_map: function () {

        switch ($('#area_type_selected').val()) {

            case 'Country':
                //if country then load region
                var region_name = locationMapleaderboard.selectedData.region.name;
                $('#main_map_hidden').attr('src', 'assets/img/map/region/' + region_name + '.png');
                $('#main_map').attr('src', 'assets/img/map/region/' + region_name + '.png');
                $('#area_type_selected').val('Region');
                $('#selected_region_name').val(region_name);
                locationMapleaderboard.createPoly('region');
                locationMapleaderboard.resize();
                $('#back_btn').show();
                break;

            case 'Region':
                //if region then load division

                $('#main_map_hidden').attr('src', 'assets/img/map/' + 'full-map-new' + '.png');
                $('#main_map').attr('src', 'assets/img/map/' + 'full-map-new' + '.png');
                $('#area_type_selected').val('Country');
                $('#selected_location_id').val(1);
                $('#selected_location_slug').val('US');
                //$('#selected_division_name').val('');
                locationMapleaderboard.createPoly('all');
                locationMapleaderboard.resize();
                break;

            case 'Devision':
                //if division then load state
                var region_name = locationMapleaderboard.selectedData.region.name;

                $('#main_map_hidden').attr('src', 'assets/img/map/region/' + region_name + '.png');
                $('#main_map').attr('src', 'assets/img/map/region/' + region_name + '.png');
                $('#area_type_selected').val('Region');
                $('#selected_location_id').val(locationMapleaderboard.selectedData.region.id);
                $('#selected_location_slug').val(locationMapleaderboard.selectedData.region.name);
                $('#selected_region_name').val(region_name);
                locationMapleaderboard.createPoly('region');
                break;
            case 'State':
                //if division then load state
                var division_name = locationMapleaderboard.selectedData.division.name.replace(/ /g, "-");
                $('#main_map_hidden').attr('src', 'assets/img/map/division/' + division_name.toUpperCase() + '.png');
                $('#main_map').attr('src', 'assets/img/map/division/' + division_name.toUpperCase() + '.png');
                $('#area_type_selected').val('Devision');
                $('#selected_location_id').val(locationMapleaderboard.selectedData.division.id);
                $('#selected_location_slug').val(locationMapleaderboard.selectedData.division.name);
                $('#selected_division_name').val(division_name);
                locationMapleaderboard.createPoly('division');
                break;

            default:
                //$('#main_map_hidden').attr('src', 'assets/img/map/full-map-new.png');
                //$('#main_map').attr('src', 'assets/img/map/full-map-new.png');
                //$('#area_type_selected').val('Country');
                break;
        }
        $('#area_type_selected').trigger('input');
        $('#selected_location_id').trigger('change');
    },

    zoom: function (obj, zoom) {
        obj.css({
            transform: 'scale(' + zoom + ')'
        });
    },
    resizeX: function (x) {
        return ((x * locationMapleaderboard.cWidth) / locationMapleaderboard.original.width);
    },
    resizeY: function (y) {
        return ((y * locationMapleaderboard.cHeight) / locationMapleaderboard.original.height);
    },
    centerThis: function (obj, xy) {
        var cx = locationMapleaderboard.cWidth / 2; //center x;
        var cy = locationMapleaderboard.cHeight / 2; // center y;
        var movex = cx - locationMapleaderboard.resizeX(xy.x);
        var movey = cy - locationMapleaderboard.resizeY(xy.y);
        locationMapleaderboard.move = { left: movex + 'px', top: movey + 'px' };
        obj.css(locationMapleaderboard.move);
        console.log('centerThis call');
        locationMapleaderboard.zoom(obj, 1.5);
        $('#location-map > .map-overlay').css(locationMapleaderboard.move);
        locationMapleaderboard.zoom($('#location-map > .map-overlay'), 1.5);
    },
    focusOn: function (type, key) {
        $('#location-map > img.full-map').addClass('deactivate');
        $('#location-map > img').removeAttr('style').removeClass('on');
        switch (type) {
            case 'region':
                $.each($('#location-map > img[cdata-region="' + key + '"]'), function (i, img) {
                    var center = $(img).data('reg-center');
                    $(img).addClass('on');
                    locationMapleaderboard.centerThis($(img), center);

                });
                break;
            case 'division':
                $.each($('#location-map > img[data-division="' + key + '"]'), function (i, img) {
                    var center = $(img).data('divi-center');
                    $(img).addClass('on');
                    //console.log(center)
                    locationMapleaderboard.centerThis($(img), center);
                });
                break;
            default:
                console.warn('Focus Type "' + type + '" is not recognaize.')

        }
        locationMapleaderboard.createPoly(type);
    },

    resizePolyXY: function (poly) {
        var array_xy = poly.split(',');
        var coords = '';
        for (i = 0; i < array_xy.length; i++) {
            if (i % 2 == 0) { // get x value
                var newx = locationMapleaderboard.resizeX(array_xy[i]);
                coords += newx + ',';
            } else { // get y value
                var newy = locationMapleaderboard.resizeY(array_xy[i]);
                coords += newy + ',';
            }
        }

        return coords.substr(0, coords.length - 1);
    },
    resize: function () {
        setTimeout(function(){
            locationMapleaderboard.cWidth = $('#location-map > .map-overlay > .map').width();
        locationMapleaderboard.cHeight = $('#location-map > .map-overlay > .map').height();
        
        $.each($('#us_mapping area'), function (i, area) {
            $(area).attr({
                coords: locationMapleaderboard.resizePolyXY($(area).data('coords'))
            })
        });
        },500)
        
    }

}
$(window).resize(function () {
    locationMapleaderboard.resize();
});
$('#selected_location_id').change(function(){
   
})
// function collect(){
//     var points = '';
//     $.each($('#location-map > .point'),function(i,p){
//         points += parseInt($(p).css('left'))+',';
//         points += parseInt($(p).css('top'))+',';
//     });
//     return points.substr(0, points.length - 1);s
// }
//  $(function(){
//      $('#location-map .map-overlay').click(function(event){
//          console.log(event.pageX,event.pageY)
//          var point = $(document.createElement('div')).addClass('point').css({
//             left:event.pageX,
//             top:event.pageY
//          });
//          $('#location-map').append(point);
//          point.click(function(){
//              point.remove();
//          })
//      });
//  });
function back_on_map_leaderboard() {
    if ($('#area_type_selected').val() == 'Region') {
        $('#back_btn').hide();
    }
    locationMapleaderboard.back_map();
}