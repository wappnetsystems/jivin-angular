import { Component, OnInit,OnDestroy,Renderer,ElementRef, Renderer2, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { DevConfig } from '../env.config';
declare var $: any;
declare var require: any;
declare var locationMap:any;
//var mapDeclearance:any = require('../../assets/js/map/regmap.js');
@Component({
  selector: 'app-signupstep3-map',
  templateUrl: './signupstep3-map.component.html',
  styleUrls: ['./signupstep3-map.component.css']
})
export class Signupstep3MapComponent implements OnInit,OnDestroy {
  @ViewChild('area_type_selected_htm',{static:true}) area_type_selected_htm: ElementRef;
	@ViewChild('selected_location_id_htm',{static:true}) selected_location_id_htm: ElementRef;
	@ViewChild('selected_location_slug_htm',{static:true}) selected_location_slug_htm: ElementRef;
  constructor(private router: Router,
		private route: ActivatedRoute,private renderer: Renderer,private http:Http) { 
    this.renderer.setElementClass(document.body, 'register1_bg', true);
  }

  errorCheck: boolean = false;
	returnUrl: string;
	loading: boolean = false;
	logData: any;

	user_region: string;
	user_division: string;
	user_state: string;

	selected_location_id = 1;
	area_type_selected = 'Country';
	selected_location_slug = 'NA';

	user_type: string = "Competitor";

	is_mobile: boolean = false;
  api_response:any={};
  ngOnInit() {
    this.logData = JSON.parse(localStorage.getItem('regForm.u_data'));
    this.loading = true;
   
    this.http.get(DevConfig.API + '/api/v1/getMapLocationLeaderboard', null)
            .subscribe((data)=>{
              this.api_response=data.json();
              locationMap.mapData=this.api_response;
              locationMap.init(locationMap.mapData);
					this.loading = false;
            },error=>{

            })

  }

  ngOnDestroy(){
    this.renderer.setElementClass(document.body, 'register1_bg', false);
  }

  show_hide_loader() {
		if($('#area_type_selected').val()=="State"){
			$('#user_type_modal').modal('show');
		}
		this.loading = true;

		setTimeout(() => {
			this.loading = false;
		}, 2000);

	}

	setMapToUser() {
		$('#user_type_modal').modal('hide');
		let mappingData =locationMap.selectedData;

		try {
			if (mappingData.division.id && mappingData.region.id && mappingData.state.id) {
				// console.log(mappingData);
				this.logData.country_id = 1;
				this.logData.region_id = mappingData.region.id;
				this.logData.division_id = mappingData.division.id;
				this.logData.state_id = mappingData.state.id;
        this.logData.user_type = this.user_type;
        
        this.http.post(DevConfig.API + 'api/v1/register', JSON.stringify(this.logData))
                      .subscribe((data)=>{
                        localStorage.setItem('regForm.u_data',JSON.stringify(this.logData));
                        this.router.navigate(['signup4']);
                      },error=>{
                        alert('Something went wrong. Try Again!');
                        this.router.navigate(['signup3']);
                      })
				



			} else {
				alert("Please choose your state from map");
			}
		} catch (error) {
			alert("Please choose your state from map");
		}


	}

}
