import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep3MapComponent } from './signupstep3-map.component';

describe('Signupstep3MapComponent', () => {
  let component: Signupstep3MapComponent;
  let fixture: ComponentFixture<Signupstep3MapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep3MapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep3MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
