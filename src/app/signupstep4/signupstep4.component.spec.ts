import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep4Component } from './signupstep4.component';

describe('Signupstep4Component', () => {
  let component: Signupstep4Component;
  let fixture: ComponentFixture<Signupstep4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
