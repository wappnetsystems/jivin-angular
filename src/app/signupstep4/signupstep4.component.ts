import { Component, OnInit, Renderer, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { WebcamComponent, WebcamInitError, WebcamImage } from 'ngx-webcam';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { DevConfig } from '../env.config';
declare var $: any;

@Component({
  selector: 'app-signupstep4',
  templateUrl: './signupstep4.component.html',
  styleUrls: ['./signupstep4.component.css']
})
export class Signupstep4Component implements OnInit, OnDestroy {
  cam_error: string = "";
  public webcamImage: WebcamImage = null;
  profile_pic_url: any = "assets/images/user_avatar.png";

  browse_image: boolean = false;
  checkSubmit: boolean = false;
  errorCheck: boolean = false;
  is_fb: boolean = false;
  is_linkedin: boolean = false;
  is_instagram: boolean = false;
  is_twitter: boolean = false;
  is_twitter_cove: boolean = false;
  is_google: boolean = false;
  coverimageUrl: any = "url('assets/images/cover_img.jpg')";
  tempUserData: any = {};
  api_response: any = {};

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  showWebcam: any = false;

  constructor(private renderer: Renderer, private router: Router, private http: Http, private element_ref: ElementRef) {

    let step_calculator = localStorage.getItem('regForm.step');
    this.http.post(DevConfig.API + 'api/v1/register', JSON.stringify(this.tempUserData))
      .subscribe((data) => {

      })
    localStorage.setItem('regForm.step', '5');
  }

  ngOnInit() {
    this.renderer.setElementClass(document.body, 'white_bg', true);
    let step_calculator = localStorage.getItem('regForm.step');
    var anythis = this;
    $('#profile_upload').on('hidden.bs.modal', function () {
      anythis.toggleWebcam();
    });

    this.tempUserData = JSON.parse(localStorage.getItem('regForm.u_data'));
    this.tempUserData.is_following = localStorage.getItem('regForm.follow');
    if (localStorage.getItem('fb_uid') && localStorage.getItem('fb_uid') != "") {
      var user_id = localStorage.getItem('fb_uid');
      this.profile_pic_url = "https://graph.facebook.com/" + user_id + "/picture?type=large";
      this.is_fb = true;
    }
    if (localStorage.getItem('linkedin_profile_image') && localStorage.getItem('linkedin_profile_image') != "") {
      this.is_linkedin = true;
      this.profile_pic_url = localStorage.getItem('linkedin_profile_image');
      this.is_fb = false;
    }
    if (localStorage.getItem('instagram_profile_image') && localStorage.getItem('instagram_profile_image') != "") {
      this.is_instagram = true;
      this.profile_pic_url = localStorage.getItem('instagram_profile_image');

    }
    if (localStorage.getItem('twitter_profile_image') && localStorage.getItem('twitter_profile_image') != "") {
      this.is_twitter = true;
      this.profile_pic_url = localStorage.getItem('twitter_profile_image');

    }
    if (localStorage.getItem('twitter_banner_image') && localStorage.getItem('twitter_banner_image') != "") {
      this.is_twitter_cove = true;
      this.checkSubmit = true;
      this.coverimageUrl = "url('" + localStorage.getItem('twitter_banner_image') + "')";

    }
    if (localStorage.getItem('google_profile_image') && localStorage.getItem('google_profile_image') != '') {
      this.is_google = true;
      this.profile_pic_url = localStorage.getItem('google_profile_image');
    }


  }

  ngOnDestroy() {
    this.renderer.setElementClass(document.body, 'white_bg', false);
  }

  public handleInitError(error: WebcamInitError): void {

    if (error.mediaStreamError && error.mediaStreamError.name === "NotAllowedError") {
      this.cam_error = "Camera access was not allowed by user!";
      alert(this.cam_error);

    }
    else {
      this.cam_error = error.message + '. Make sure you have webcam on if you are accessing site from computer.';
      alert(this.cam_error);
    }
  }

  public handleImage(webcamImage: WebcamImage): void {
    //this.showWebcam=false;
    console.log('received webcam image', webcamImage);
    this.profile_pic_url = webcamImage.imageAsDataUrl;
    this.browse_image = true;
    this.webcamImage = webcamImage;
    $('#profile_upload').modal('hide');

    this.is_fb = false;
    this.is_linkedin = false;
    this.is_instagram = false;
    this.is_twitter = false;
    this.is_google = false;

  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public onWebcam(): void {

    this.showWebcam = true;
  }

  public triggerSnapshot(): void {

    this.trigger.next();
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  changeListner(event: any) {

    this.is_fb = false;
    this.is_linkedin = false;
    this.is_instagram = false;
    this.is_twitter = false;
    this.is_google = false;
    let reader = new FileReader();
    let image = this.element_ref.nativeElement.querySelector('.profilepic');
    let file = event.target.files[0];

    if (file === undefined) {
      //image.src = "assets/img/pp.png";
      alert("Please choose an image file!");
      return;
    } else if (file.size >= 2 * 1024 * 1024) {

      //image.src = "assets/img/pp.png";
      alert("Please upload images of maximum 10MB");
      return;

    } else if (!file.type.match('image.*')) {
      // image.src = "assets/img/pp.png";
      alert("Please choose an image file!");
    } else {

      this.checkSubmit = true;

      reader.onload = (e) => {
        let src = reader.result;
        this.profile_pic_url = src;

        //image.src = src;

      };

      reader.readAsDataURL(event.target.files[0]);
      $("#profile_upload").modal("hide");
    }

  }
  chooseImgCover() {
    this.element_ref.nativeElement.querySelector('.file-input-cover').click();
  }
  changeListnerCover(event: any) {

    this.is_twitter_cove = false;
    let reader = new FileReader();
    let file = event.target.files[0];
    //let matchType = ['jpg' , 'jpeg', 'png', 'bmp', 'gif'];
    //let ext = event.target.value.split('.');
    //ext = ext[ext.length-1].toLowerCase(); 
    this.checkSubmit = false;

    if (file === undefined) {
      this.coverimageUrl = "url('assets/img/cover_img.jpg')";
      alert("Please choose an image file!");

    } else if (!file.type.match('image.*')) {
      this.coverimageUrl = "url('assets/img/cover_img.jpg')";
      alert("Please choose an image file!");
    } else {

      this.checkSubmit = true;

      reader.onload = (e) => {
        let src = reader.result;
        this.coverimageUrl = "url(" + src + ")";

      };

      reader.readAsDataURL(event.target.files[0]);
      $("#cover_upload").modal("hide");
    }
  }

  saveProfilePic() {

    // console.log("<<<lllll>>>",this.webcamimg);
    if (this.webcamImage) {
      this.checkSubmit = true;

    }

    //console.log(this.element.nativeElement.querySelector('.file-input').files[0]);
    if (this.checkSubmit == true) {

      ////this.tempUserData = JSON.parse(localStorage.getItem('regForm.u_data'));
      //this.sendRequestImage.user_id = this.tempUserData.id;
      //this.sendRequestImage.image = this.element.nativeElement.querySelector('.file-input').files[0];
      //this.sendRequestImage.is_temporary = 1;
      let formData = new FormData();
      if (this.webcamImage) {

        formData.append("webcamimg", '1');
        formData.append("image", this.webcamImage.imageAsDataUrl);
        formData.append('is_fb', '0');
        formData.append('is_linkedin', '0');
        formData.append('is_instagram', '0');
        formData.append('is_twitter', '0');
        formData.append('is_google', '0');
      }
      else if (this.is_fb) {
        formData.append('is_fb', '1');
        formData.append("image", this.profile_pic_url);
      }
      else if (this.is_linkedin) {
        formData.append('is_linkedin', '1');
        formData.append("image", this.profile_pic_url);
      }
      else if (this.is_instagram) {
        formData.append('is_instagram', '1');
        formData.append("image", this.profile_pic_url);
      }
      else if (this.is_twitter) {
        formData.append('is_twitter', '1');
        formData.append("image", this.profile_pic_url);
      }
      else if (this.is_google) {
        formData.append('is_google', '1');
        formData.append("image", this.profile_pic_url);
      }
      else {
        formData.append('is_fb', '0');
        formData.append('is_linkedin', '0');
        formData.append("webcamimg", '0');
        formData.append('is_instagram', '0');
        formData.append('is_twitter', '0');
        formData.append('is_google', '0');
        formData.append("image", this.element_ref.nativeElement.querySelector('.file-input').files[0]);


      }

      if (this.is_twitter_cove) {
        formData.append('is_twitter_cover', '1');
        formData.append("cover_image", this.coverimageUrl);
      }
      else {
        formData.append('is_twitter_cover', '0');
        formData.append("cover_image", this.element_ref.nativeElement.querySelector('.file-input-cover').files[0]);
      }


      formData.append("user_id", this.tempUserData.user_id);
      // formData.append("image", this.element.nativeElement.querySelector('.file-input').files[0]);
      formData.append("is_temporary", '1');
      this.http.post(DevConfig.API + '/api/v1/add-user-profile-image', formData)
        .subscribe((data) => {
          this.api_response = data.json();
          if (this.api_response.success) {
            localStorage.removeItem('linkedin_profile_image');
            localStorage.removeItem('fb_uid');
            localStorage.removeItem('instagram_profile_image');
            localStorage.removeItem('twitter_profile_image');
            localStorage.removeItem('twitter_banner_image');
            localStorage.removeItem('google_profile_image');

            this.router.navigate(['/signup5']);

          }
          else {
            alert('Something went wrong. Please try again!');
            this.errorCheck = false;
          }
        }, error => {
          alert('Something went wrong. Please try again!');
          this.errorCheck = false;
        });


    } else {
      this.errorCheck = false;
    }

    setTimeout(() => {
      this.errorCheck = true;
    }, 5000);
  }


}
