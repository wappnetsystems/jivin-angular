import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep1Component } from './signupstep1.component';

describe('Signupstep1Component', () => {
  let component: Signupstep1Component;
  let fixture: ComponentFixture<Signupstep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
