import { Component, OnInit,Renderer,OnDestroy } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-signupstep1',
  templateUrl: './signupstep1.component.html',
  styleUrls: ['./signupstep1.component.css']
})
export class Signupstep1Component implements OnInit,OnDestroy {

  constructor(private renderer:Renderer,private router:Router) { 
    this.renderer.setElementClass(document.body, 'register1_bg', true);
    let step_calculator = localStorage.getItem('regForm.step');
  }
  userInformation:any={};
  nameData:string="";
  ngOnInit() {
    this.userInformation = JSON.parse(localStorage.getItem('regForm.u_data'));
    if(this.userInformation == undefined || this.userInformation.name == undefined || this.userInformation.email == undefined) {
			this.router.navigate(['']);
		}
    this.nameData=this.userInformation.name;
    localStorage.setItem("regForm.t",this.userInformation.user_id);
		localStorage.setItem('regForm.step', '2');
  }
  ngOnDestroy(){
    this.renderer.setElementClass(document.body, 'register1_bg', false);
  }

}
