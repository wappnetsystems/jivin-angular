import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CommonApiService } from '../common-api.service';
import { AsyncdataService } from '../asyncdata.service';
declare var $: any
@Component({
  selector: 'app-sidepanel',
  templateUrl: './sidepanel.component.html',
  styleUrls: ['./sidepanel.component.css']
})
export class SidepanelComponent implements OnInit {
  stateName: any;
  public imageData: any;
  userInformation: any;
  totalFollowers: number;
  totalEvent: number;
  eventObjects: any;
  userInterests: string;
  public siteUrl: any;

  tempVideoArr: any;
  tempAudioArr: any;
  is_compitator: boolean = true;
  video_votes: any = 0;
  audio_votes: any = 0;
  total_votes: any = 0;
  current_route: any = "";
  my_video_list: any = "";
  my_audio_list: any = "";

  constructor(private router: Router, private commonService: CommonApiService,
    private asyncdataService: AsyncdataService) {
    this.userInformation = JSON.parse(localStorage.getItem('currentUser'));

    this.tempVideoArr = new Array(this.userInformation.user_info.total_videos);

    this.tempAudioArr = new Array(this.userInformation.user_info.total_audios);

    this.totalFollowers = this.userInformation.user_info.total_follows;

    this.totalEvent = this.userInformation.user_info.total_events;
    this.eventObjects = this.userInformation.user_info.events;
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.siteUrl = event.url;
      }
    })
    this.current_route = this.router.url;

  }

  ngOnInit() {
    this.get_votes_count();
    if (this.userInformation.user_info.user_type == "Fan") {

      this.is_compitator = false;
    }
    else {
      this.is_compitator = true;
    }

    this.userInterests = this.userInformation.user_info.interests;
    this.userInterests = this.userInterests.substr(0, 50) + "...";

    this.asyncdataService.getTotalFollowers().subscribe((count) => {
      this.totalFollowers = count;

    });

    this.asyncdataService.getTotalEvent().subscribe((count) => {
      this.totalEvent = count;
    });

    this.asyncdataService.getEventObject().subscribe((data) => {
      this.eventObjects = data;
    });

    this.asyncdataService.getVotesCount().subscribe((data) => {
      this.userInformation.user_info.total_votes = data;
      localStorage.setItem('currentUser', JSON.stringify(this.userInformation));
    });
    this.asyncdataService.getAudioVotesCount().subscribe((data) => {
      this.userInformation.user_info.total_audio_votes = data;
      localStorage.setItem('currentUser', JSON.stringify(this.userInformation));
    });
    this.asyncdataService.getVideoVotesCount().subscribe((data) => {
      this.userInformation.user_info.total_video_votes = data;
      localStorage.setItem('currentUser', JSON.stringify(this.userInformation));
    });

    this.asyncdataService.getFollowedByObject().subscribe((data) => {
      this.userInformation.user_info.total_followed_by = data.followed_by_count;
      localStorage.setItem('currentUser', JSON.stringify(this.userInformation));
    });

    this.asyncdataService.getTotalVideos().subscribe((data) => {

      if (data.type === 'add') {
        this.userInformation.user_info.total_videos = data.count;
      } else if (data.type === 'remove') {
        this.userInformation.user_info.total_videos = parseInt(this.userInformation.user_info.total_videos) > 0 ? parseInt(this.userInformation.user_info.total_videos) - data.count : 0;
      }
      localStorage.setItem('currentUser', JSON.stringify(this.userInformation));

      this.tempVideoArr = new Array(this.userInformation.user_info.total_videos);

    });

    this.asyncdataService.getTotalAudios().subscribe((data) => {

      if (data.type === 'add') {
        this.userInformation.user_info.total_audios = data.count;
      } else if (data.type === 'remove') {
        this.userInformation.user_info.total_audios = parseInt(this.userInformation.user_info.total_audios) > 0 ? parseInt(this.userInformation.user_info.total_audios) - data.count : 0;
      }
      localStorage.setItem('currentUser', JSON.stringify(this.userInformation));
      this.tempAudioArr = new Array(this.userInformation.user_info.total_audios);
    });

    var userstate = this.userInformation.user_info.design_settings.state.slug;
    this.stateName = this.userInformation.user_info.design_settings.state.name;
    var currentDate = new Date();
    var mont = currentDate.getMonth() + 1
    var totaldate2 = currentDate.getFullYear() + "-" + mont + "-" + currentDate.getDate();
    var totalhour = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();

    let aData = { date: totaldate2, time: totalhour, state_id: userstate };

    this.commonService.getAdimage(aData, this.userInformation.api_key)
      .subscribe(
        (data: any) => {
          this.imageData = data;

        }
      );

    let pf_data = { username: this.userInformation.user_info.username, current_user_id: this.userInformation.user_info.id };
    this.commonService.getProfileDetails(pf_data)  //call for get all country lists
      .subscribe(
        (data: any) => {
          if (data != false) {
            this.userInformation.user_info = data.user_info;
            localStorage.setItem('currentUser', JSON.stringify(this.userInformation));
          } else {
            this.router.navigate(['/']);
          }
        });

    this.commonService.get_video_post_by_user_id(this.userInformation.user_info.id, this.userInformation.api_key)
      .subscribe((data: any) => {

        this.my_video_list = data.data;

      }, error => {
        console.log('error in get_video_post_by_user_id api');
      });

    this.commonService.get_audio_post_by_user_id(this.userInformation.user_info.id, this.userInformation.api_key)
      .subscribe((data: any) => {

        this.my_audio_list = data.data
      }, error => {
        console.log('error in get_audio_post_by_user_id api');
      });


  }

  get_votes_count() {

    this.commonService.get_total_video_votes(this.userInformation.api_key, this.userInformation.user_info.id)
      .subscribe((data: any) => {

        if (data.success) {

          this.video_votes = data.data.video_votes;

        }
        else {
          this.video_votes = 0;
        }
      }, error => {
        this.video_votes = 0;
      });
    this.commonService.get_total_audio_votes(this.userInformation.api_key, this.userInformation.user_info.id)
      .subscribe((data: any) => {

        if (data.success) {
          this.audio_votes = data.data.audio_votes;
        }
        else {
          this.audio_votes = 0;
        }
      }, error => {
        this.audio_votes = 0;
      });
  }

}
