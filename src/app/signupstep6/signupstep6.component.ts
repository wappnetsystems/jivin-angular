import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, NgModule, Input, AfterViewChecked, Renderer, OnDestroy } from '@angular/core';
//import { CeiboShare } from 'ng2-social-share';
import { Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { DevConfig } from '../env.config';
declare let gapi: any;
@Component({
  selector: 'app-signupstep6',
  templateUrl: './signupstep6.component.html',
  styleUrls: ['./signupstep6.component.css'],
  //providers:[CeiboShare],
})
export class Signupstep6Component implements OnInit,OnDestroy {
  yahooShareUrl: string;
	outlookShareUrl: string;
	gmailShareUrl: string;
	auth2: any;
	public linkedInShareUrl: string;
	public repoUrl = DevConfig.BASE_URL;
	loading:any=false;
	public imageUrl = DevConfig.BASE_URL + 'assets/img/logo.png';
	agreeterms = false;
	erroMsg :any = "";
  constructor( private router: Router,
    private renderer: Renderer, private http: Http) { 
      this.renderer.setElementClass(document.body, 'white_bg', true);
      let step_calculator = localStorage.getItem('regForm.step');
  }
  userInfo: any = JSON.parse(localStorage.getItem('regForm.u_data'));
  ngOnInit() {
    localStorage.setItem('regForm.step', '7');

  }
  ngOnDestroy(){
		this.renderer.setElementClass(document.body, 'white_bg', false);
  }
  
  shareOnLinkedIn() {
		// makes the url and titles URI safe
		let encodedUrl = encodeURIComponent(this.repoUrl);
		let encodedTitle = encodeURIComponent("Welcome To Jivin");
		let encodedSummary = encodeURIComponent("Let The Competition Begin!");
		let encodedSource = encodeURIComponent("JIVIN");
	
		this.linkedInShareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${encodedUrl}&title=${encodedTitle}&summary=${encodedSummary}&source=${encodedSource}`;
	
		// In some versions the popup won't open unless it's being used to initialize a variable.
		let popup = window.open(this.linkedInShareUrl, "myWindow", 'width=640,height=480');
	}

	gmail() {
		// gapi.auth2.getAuthInstance().signIn({
		// 	client_id: '1040453577892-2hrd60k504758lq0bmnrh4lv4hiepics.apps.googleusercontent.com',
		// 	cookie_policy: 'single_host_origin',
		// 	scope: 'profile email https://www.googleapis.com/auth/contacts'
		// }).then(
		// 	(data: any) => {
		// 		console.log(data);
		// 	}
		// );

		let gmailclient = '172760255594-p355fqrcq2pauimc7vqo8ngqbsmb98j3.apps.googleusercontent.com';
		//let gmailredirecturl = 'https://jivin.com/services/vendor/gmail/callback.php';
		let gmailredirecturl = 'https://jivin.com/development/jivin/services/vendor/gmail/callback.php';
		this.gmailShareUrl = `https://accounts.google.com/o/oauth2/auth?client_id=${gmailclient}&redirect_uri=${gmailredirecturl}&scope=https://www.google.com/m8/feeds/&response_type=code`;
		
			// In some versions the popup won't open unless it's being used to initialize a variable.
		let popup = window.open(this.gmailShareUrl, "myWindow", 'width=640,height=480');
	}
	outlook(){
		let outlookclient = 'c6422c82-2656-4ec7-8c19-c04b90d02fe0';
		let outlookredirecturl = 'https://jivin.com/development/jivin/services/vendor/hotmail/oauth-hotmail.php';

		this.outlookShareUrl = `https://login.live.com/oauth20_authorize.srf?client_id=${outlookclient}&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri=${outlookredirecturl}`;
		
			// In some versions the popup won't open unless it's being used to initialize a variable.
			let popup = window.open(this.outlookShareUrl, "myWindow", 'width=640,height=480');
	}
	yahoo(){
		this.yahooShareUrl = `https://jivin.com/services/public/yahoo/`;
		
			// In some versions the popup won't open unless it's being used to initialize a variable.
			let popup = window.open(this.yahooShareUrl, "myWindow", 'width=640,height=480');
	}

	fetchmail() {
		gapi.load('client:auth2', () => {
			gapi.client.init({
				apiKey: 'API_KEY use your own',
				discoveryDocs: ['https://people.googleapis.com/$discovery/rest?version=v1'],
				clientId: '1040453577892-2hrd60k504758lq0bmnrh4lv4hiepics.apps.googleusercontent.com',
				scope: 'profile email https://www.googleapis.com/auth/contacts.readonly'
			}).then(() => {
				return gapi.client.people.people.connections.list({
					resourceName:'people/me',
					personFields: 'emailAddresses,names'
				});
			}).then(
				(res:any) => {
					console.log("Res: " + JSON.stringify(res));
					// this.userContacts.emit(this.transformToMailListModel(res.result));
				},
				(error:any) => console.log("ERROR " + JSON.stringify(error))
			);
		});
	}

	checkagree($ev:any) {
		console.log($ev);
		
		if($ev.target.checked) {
			this.agreeterms = true;
		} else {
			this.agreeterms = false;
		}
	}

	nextstep() {
		// this.erroMsg = "";
		// if(!this.agreeterms) {
		// 	this.erroMsg = 'Please agree to the terms and condition.';
		// 	return;
		// } else {
			this.router.navigate(['/signup7']);
		// }
	}

}
