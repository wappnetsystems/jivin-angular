import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep6Component } from './signupstep6.component';

describe('Signupstep6Component', () => {
  let component: Signupstep6Component;
  let fixture: ComponentFixture<Signupstep6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
