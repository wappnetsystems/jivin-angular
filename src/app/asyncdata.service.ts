import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
@Injectable({
  providedIn: 'root'
})
export class AsyncdataService {

  private dpSubject : Subject<string>;
    private colorPickerSubject : Subject<string>;
    private totalfollwers : Subject<number>;
    private followedByObject : Subject<any>;
    private followersObject : Subject<any>;
    private backgroundImage : Subject<any>;
    private totalEvents : Subject<number>;
    private eventObject : Subject<any>;
    private bidObject : Subject<any>;

    private totalVideos : Subject<any>;
    private totalAudios : Subject<any>;
    
    private totalVotes : Subject<any>;
    private totalAudioVotes : Subject<any>;
    private totalVideoVotes : Subject<any>;

    private topAreaUser: Subject<any>;

    constructor() {
        this.dpSubject = new Subject<string>();
        this.colorPickerSubject = new Subject<string>();
        this.totalfollwers = new Subject<number>();
        this.followedByObject = new Subject<any>();
        this.followersObject = new Subject<any>();
        this.backgroundImage = new Subject<any>();
        this.totalEvents = new Subject<number>();
        this.eventObject = new Subject<any>();
        this.bidObject = new Subject<any>();

        this.totalVotes = new Subject<any>();
        this.totalAudioVotes = new Subject<any>();
        this.totalVideoVotes = new Subject<any>();

        this.totalVideos = new Subject<any>();
        this.totalAudios = new Subject<any>();

        this.topAreaUser = new Subject<any>();
    }

    setLogo(logoUrl: string): void {

        this.dpSubject.next(logoUrl);
    }

    getLogo(): Observable<string> {

        return this.dpSubject;
    }
    setPickerColor(color:string) {
         this.colorPickerSubject.next(color);   
    }
    getPickerColor(): Observable<string> {
        return this.colorPickerSubject;
    }
    setTotalFollowers(count:number) {

         this.totalfollwers.next(count);   
    }
    getTotalFollowers(): Observable<number> {

        return this.totalfollwers;
    }
    setFollowedByObject(obj:any) {

        this.followedByObject.next(obj);   
    }
    getFollowedByObject(): Observable<any> {
        return this.followedByObject;
    }

    setFollowersObject(data:any) {
        console.log("hello janina");
         this.followersObject.next(data);   
    }
    getFollowersObject(): Observable<any> {

        return this.followersObject;
    }
    setBackground(data:any) {

         this.backgroundImage.next(data);   
    }
    getBackground(): Observable<any> {

        return this.backgroundImage;
    }
    setTotalEvent(data:number) {

         this.totalEvents.next(data);   
    }
    getTotalEvent(): Observable<number> {

        return this.totalEvents;
    }
    setEventObject(data:any) {

         this.eventObject.next(data);   
    }
    getEventObject(): Observable<any> {

        return this.eventObject;
    }

    // FOR VOTES COUNT
    setVotesCount(allVote:any) {
         this.totalVotes.next(allVote);   
    }
    getVotesCount(): Observable<any> {
        return this.totalVotes;
    }

    setAudioVotesCount(audioVote:any) {
         this.totalAudioVotes.next(audioVote);   
    }
    getAudioVotesCount(): Observable<any> {
        return this.totalAudioVotes;
    }
    setVideoVotesCount(videoVote:any) {
         this.totalVideoVotes.next(videoVote);   
    }
    getVideoVotesCount(): Observable<any> {
        return this.totalVideoVotes;
    }

    setBidObject(data:any) {
        this.bidObject.next(data);   
    }
    getBidObject(): Observable<any> {
        return this.bidObject;
    }

    setTotalVideos(data:any) {
        this.totalVideos.next(data);   
    }
    getTotalVideos(): Observable<any> {
        return this.totalVideos;
    }

    setTotalAudios(data:any) {
        this.totalAudios.next(data);   
    }
    getTotalAudios(): Observable<any> {
        return this.totalAudios;
    }

    setTopAreaUser(data:any) {
        this.topAreaUser.next(data);   
    }
    getTopAreaUser(): Observable<any> {
        return this.topAreaUser;
    }
}
