import { Component, OnInit, NgZone,ViewChild,AfterViewChecked } from '@angular/core';
import { Injectable } from '@angular/core';
import { NgForm, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PlatformLocation } from '@angular/common';
import { DevConfig } from '../env.config';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';
import { AuthService, SocialUser } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
declare var $: any;
declare global {
  interface Window { angularComponentRef: any; }
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,AfterViewChecked {
  private g_user: SocialUser;
  public forgetPassForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  model: any = {};
  loading = false;
  returnUrl: string;
  errorCheck: boolean = false;
  message: string;
  successMessage: boolean = false;
  floading_ico: boolean = false;
  ferror_msg: string = "Invalid email id given..Please try again..";
  ferror: boolean = false;
  msg: string = "";
  base_url: string;
  baseUrl: string;
  socialdata: any;
  twitterData: any = {};
  public sub: any;
  api_response: any = {};
  client_id = "81lq3vo74v13dv";
  client_secret = "U8aVqvUW1hEi2FQM";

  instagram_client_id = "2fb5b86517134bd7a9a0cb8e0d8e2792";
  instagram_client_secret = "8b3ddbd6460c46f7ba7f4d615d4183eb";
//var for registration start
regdata: any = { name: '', email: '' };
	formErrors: any;
	errorMessage: string;
	
	userData: any = {
		user_id: 0,
		finish_reg: 0,
		name: "",
		email: "",
		username: "",
		birth_month: "",
		birth_year: "",
		password: "",
		user_interests: "",
		is_following: "",
		secondary_email: "",
		imgb64string: "",
		country_id: "",
		region_id: "",
		division_id: "",
		state_id: ""
	};

	validationMessages: any = {
		'name': {
			'required': 'Name is required.',
			// 'minlength':     'Name must be at least 4 characters long.',
			// 'maxlength':     'Name cannot be more than 24 characters long.',
			// 'forbiddenName': 'Someone named "Bob" cannot be a hero.'
		},
		'email': {
			'required': 'Email is required.',
			'email': 'Please provide a valid Email'
		}
	};
  regForm: NgForm;
	@ViewChild('regForm',{static: true}) currentForm: NgForm;
//var for registration end
  constructor(private http: Http, private router: Router, private _fb: FormBuilder,
    private facebook: FacebookService, private authService: AuthService, private zone: NgZone) {

    this.base_url = DevConfig.BASE_URL + "password/reset";
    this.baseUrl = DevConfig.BASE_URL;

    let initParams: InitParams = {
      appId: '308438263165220',
      //xfbml: true,
      version: 'v3.2'
    };

    facebook.init(initParams);
    window.angularComponentRef = window.angularComponentRef || {};
    window.angularComponentRef.setLoginLinkedin = this.setLoginLinkedin.bind(this);
    window.angularComponentRef.setLoginInstagram = this.setLoginInstagram.bind(this);
    window.angularComponentRef.setLoginTwitter = this.setLoginTwitter.bind(this);


  }

  ngOnInit() {
    this.forgetPassForm = this._fb.group({
      email: ['', [<any>Validators.required, <any>Validators.email]],
    });

    // subscribe to form changes  
    this.subcribeToFormChanges();
  }

  setLoginLinkedin(code: any) {

    this.zone.run(() => this.linkedinAuth(code));

  }
  setLoginInstagram(code: any) {
    this.zone.run(() => this.instagramAuth(code));
  }

  setLoginTwitter(code: any) {
    this.zone.run(() => this.twitterAuth(code));
  }

  subcribeToFormChanges() {

    const myFormStatusChanges$ = this.forgetPassForm.statusChanges;
    const myFormValueChanges$ = this.forgetPassForm.valueChanges;

    myFormStatusChanges$.subscribe(x => this.events.push({ event: 'STATUS_CHANGED', object: x }));
    myFormValueChanges$.subscribe(x => this.events.push({ event: 'VALUE_CHANGED', object: x }));
  }

  logeerdIn(provider: any) {

    if (provider == "linkedin") {
      var options = '';
      options += ',width=600px';
      options += ',height=600px';

      window.open("https://www.jivin.com/development/jivin/services/public/linkedin_login/1", "LinkedIn Authentication", options);
    }
    else if (provider == "facebook") {
      this.facebookAuth();
    }
    else if (provider == "instagram") {
      var options = 'left=200px';
      options += ',width=600px';
      options += ',height=600px';

      window.open("https://www.jivin.com/development/jivin/services/public/instagram_login/1", "Instagram Authentication", options);
    }
    else if (provider == "twitter") {
      var options = 'left=200px';
      options += ',width=600px';
      options += ',height=600px';

      window.open("https://www.jivin.com/development/jivin/services/public/api/v1/get_access_token_twitter/1", "Twitter Authentication", options);
    }
    else {
      var new_this=this;
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(function (data) {
        
        new_this.loginapi(data.email, '', provider)
      });

    }

  }
  twitterAuth(code: any) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let token_data={'temp_id':code};
    this.http.post(DevConfig.API + 'api/v1/get_user_profile_twitter', token_data, options)
      .subscribe((data) => {
        this.api_response = data.json();
        if (this.api_response.success && this.api_response.data.email) {
          this.loginapi(this.api_response.data.email, '', 'twitter')
        }
        else {
          alert('Something went wrong. Try Again!');
        }
      }, error => {
        alert('Something went wrong. Try Again!');
      })

    
  }

  linkedinAuth(code: any) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let linkedin_data = { 'flag': 1, 'grant_type': 'authorization_code', 'code': code, 'client_id': this.client_id, 'client_secret': this.client_secret };
    this.http.post(DevConfig.API + 'api/v1/get_access_token_linkedin', linkedin_data, options)
      .subscribe((data) => {
        this.api_response = data.json();
        if (this.api_response.success && this.api_response.data.access_token) {
          var access_token = this.api_response.data.access_token;
          let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          let token_data = { 'access_token': access_token };
          let options = new RequestOptions({ headers: headers });
          return this.http.post(DevConfig.API + 'api/v1/get_user_email_linkedin', token_data, options)
            .subscribe((data) => {
                this.api_response=data.json();
                if(this.api_response.success){
                  this.loginapi(this.api_response.data.elements[0]['handle~']['emailAddress'], '', 'linkedin')
                }
                else{
                  alert('Something went wrong. Try Again!');
                }
            })
          
        }
        else {
          alert('Something went wrong. Try Again!');
        }
      }, error => {
        alert('Something went wrong. Try Again!');
      })

  }

  instagramAuth(code: any) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let instagram_data = { 'flag': 1, 'grant_type': 'authorization_code', 'code': code, 'client_id': this.instagram_client_id, 'client_secret': this.instagram_client_secret };
    this.http.post(DevConfig.API + 'api/v1/get_access_token_instagram', instagram_data, options)
      .subscribe((data) => {
        this.api_response = data.json();
        if (this.api_response.success && this.api_response.data.access_token) {
          this.loginapi(this.api_response.data.user.username, '', 'instagram')
        }
        else {
          alert('Something went wrong. Try Again!');
        }
      }, error => {
        alert('Something went wrong. Try Again!');
      })


  }
  facebookAuth() {
    const options: LoginOptions = {
      scope: 'public_profile,user_friends,email,pages_show_list',
      return_scopes: true,
      enable_profile_selector: true
    };
    this.facebook.login(options)
      .then((response: LoginResponse) => {
        console.log(JSON.stringify(response));
        this.facebook.api('/me?fields=id,name,email')
          .then(res => {
            console.log(res);
            if (res.email) {
              this.loginapi(res.email, '', 'facebook');

            }
            else {
              this.errorCheck = true;
              this.message = "Email id is necessary to signup with facebook. Please allow permission to access your facebook email.";

            }

          })
          .catch(e => console.log(e));
      })
      .catch((error: any) => console.error(error));

  }

  login() {
    
    this.loginapi(this.model.username, this.model.password, '');


  }
  loginapi(username: string, password: string, logintype: any) {
    this.loading=true;
    this.http.post(DevConfig.API + 'api/v1/login', JSON.stringify({ username: username, password: password, logintype: logintype }))
      .subscribe((val) => {
        this.loading=false;
        this.api_response = val.json();
        let user = this.api_response;
        if (user && user.success == 1 && user.data.api_key) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user.data));
          this.router.navigate(['/wall']);
        }
        else {
          this.errorCheck = true;
          this.message = this.api_response.msg;
          this.model.username = "";
          this.model.password = "";
        }


      },
        error => {
          this.loading=false;
          alert('Something went wrong. Try Again.');
        }
      );
  }
  sendEmail(value: any, isValid: boolean) {
    this.submitted = true;
    if (isValid) {
      let emailObject = { user_name_or_email: value.email, url: this.base_url };
      this.http.post(DevConfig.API + '/api/v1/forget_password', emailObject)
        .subscribe((val) => {
          this.api_response = val.json();
          if (this.api_response.success == 0) {
            this.ferror = true;
            if (this.api_response.msg == "Server Error") {
              this.ferror_msg = "Internal server error occured..please try later!";
            }
            return;
          } else {
            this.successMessage = true;
            this.ferror = false;
            return;
          }
        }, error => {
          alert('Something went wrong. Try Again.');
          return;
        })
    }
    else {
      alert("Something went wrong. Try Again!");
      return;
    }
  }
  
//code for registration start


ngAfterViewChecked() {
  this.formChanged();
}

formChanged() {
  if (this.currentForm === this.regForm) { return; }
  this.regForm = this.currentForm;
  if (this.regForm) {
    this.regForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
  }
}

onValueChanged(data?: any) {
  if (!this.regForm) { return; }
  const form = this.regForm.form;

  for (const field in this.formErrors) {
    // clear previous error message (if any)
    this.formErrors[field] = '';
    const control = form.get(field);

    if (control && control.dirty && !control.valid) {
      const messages = this.validationMessages[field];
      for (const key in control.errors) {
        this.formErrors[field] += messages[key] + ' ';
      }
    }
  }
}

//code for registration end


}
