import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdChoicesComponent } from './ad-choices.component';

describe('AdChoicesComponent', () => {
  let component: AdChoicesComponent;
  let fixture: ComponentFixture<AdChoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdChoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdChoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
