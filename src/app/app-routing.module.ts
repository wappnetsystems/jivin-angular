import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { WallComponent } from './wall/wall.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { LanguagesComponent } from './languages/languages.component';
import { CookiesComponent } from './cookies/cookies.component';
import { AdvertisingComponent } from './advertising/advertising.component';
import { AdChoicesComponent } from './ad-choices/ad-choices.component';
import { Signupstep1Component} from './signupstep1/signupstep1.component';
import { Signupstep2Component } from './signupstep2/signupstep2.component';
import { Signupstep3Component } from './signupstep3/signupstep3.component';
import { Signupstep3MapComponent} from './signupstep3-map/signupstep3-map.component';
import { Signupstep4Component } from './signupstep4/signupstep4.component';
import { Signupstep5Component } from './signupstep5/signupstep5.component';
import { Signupstep6Component } from './signupstep6/signupstep6.component';
import { Signupstep7Component } from './signupstep7/signupstep7.component';
const routes: Routes = [
  {
    path:"",
    component:LoginComponent
  },
  {
    path:"",
    component:LoginComponent
  },
  {
    path:"wall",
    component:WallComponent
  },
  {
    path:"privacy",
    component:PrivacyComponent
  },
  {
    path:"terms",
    component:TermsComponent
  },
  {
    path:"advertising",
    component:AdvertisingComponent
  },
  {
    path:"ad-choices",
    component:AdChoicesComponent
  },
  {
    path:"cookies",
    component:CookiesComponent
  },
  {
    path:"language-link",
    component:LanguagesComponent
  },
  {
    path:"signup1",
    component:Signupstep1Component
  },
  {
    path:"signup2",
    component:Signupstep2Component
  },
  {
    path:"signup3",
    component:Signupstep3Component
  },
  {
    path:"signup3-map",
    component:Signupstep3MapComponent
  },
  {
    path:"signup4",
    component:Signupstep4Component
  },
  {
    path:"signup5",
    component:Signupstep5Component
  },
  {
    path:"signup6",
    component:Signupstep6Component
  },
  {
    path:"signup7",
    component:Signupstep7Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
