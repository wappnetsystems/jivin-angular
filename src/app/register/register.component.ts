import { Component, OnInit, ViewChild, NgZone, ElementRef, AfterViewInit, NgModule, Input, AfterViewChecked, OnDestroy } from '@angular/core';
import { FormControl, NgForm, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DevConfig } from '../env.config';
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';
import { Http } from '@angular/http';
import { SocialMediaService } from '../social-media.service';
import { AuthService, GoogleLoginProvider } from 'angularx-social-login';
declare var $: any;
declare global {
	interface Window { angularComponentRef: any; }
}
declare const gapi: any;

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements AfterViewChecked, OnDestroy, OnInit {
	baseUrl: string;
	public fbuser: any;
	public sub: any;
	public auth2: any;

	linkedin_redirect_url = "https://www.jivin.com/development/jivin/services/public/linkedin_auth";

	client_id = "81lq3vo74v13dv";
	client_secret = "U8aVqvUW1hEi2FQM";

	instagram_client_id = "2fb5b86517134bd7a9a0cb8e0d8e2792";
	instagram_client_secret = "8b3ddbd6460c46f7ba7f4d615d4183eb";

	regForm: NgForm;
	@ViewChild('regForm', { static: true }) currentForm: NgForm;

	api_response: any = {};
	regdata: any = { name: '', email: '' };
	formErrors: any;
	errorMessage: string;
	errorCheck: boolean = false;
	userData: any = {
		user_id: 0,
		finish_reg: 0,
		name: "",
		email: "",
		username: "",
		birth_month: "",
		birth_year: "",
		password: "",
		user_interests: "",
		is_following: "",
		secondary_email: "",
		imgb64string: "",
		country_id: "",
		region_id: "",
		division_id: "",
		state_id: ""
	};

	validationMessages: any = {
		'name': {
			'required': 'Name is required.',

		},
		'email': {
			'required': 'Email is required.',
			'email': 'Please provide a valid Email'
		}
	};

	constructor(private http: Http, private router: Router, private facebook: FacebookService,
		private zone: NgZone, private social_media_service: SocialMediaService,
		private authService: AuthService) {
		window.angularComponentRef = window.angularComponentRef || {};
		window.angularComponentRef.setLinkedin = this.setLinkedin.bind(this);
		window.angularComponentRef.setInstagram = this.setInstagram.bind(this);
		window.angularComponentRef.setTwitter = this.setTwitter.bind(this);
		let initParams: InitParams = {
			appId: '308438263165220',
			xfbml: true,
			version: 'v3.2'
		};

		facebook.init(initParams);
	}

	setLinkedin(code: any) {

		this.zone.run(() => this.linkedinAuth(code));

	}
	setInstagram(code: any) {
		this.zone.run(() => this.instagramAuth(code));
	}

	setTwitter(code: any) {
		this.zone.run(() => this.twitterAuth(code));
	}

	twitterAuth(code: any) {
		this.social_media_service.get_user_twitter_profile(code)
			.subscribe((data: any) => {
				if (data.success) {
					if (data.data.email) {
						this.userData.name = data.data.name;
						this.userData.social_uid = data.data.twitter_id;
						let profile_image_data = data.data.profile_image;
						localStorage.setItem('twitter_profile_image', data.data.profile_image);
						localStorage.setItem('twitter_banner_image', data.data.banner_image);
						this.userData.email = data.data.email;
						this.userData.registrationtype = 'twitter';
						this.social_media_service.regSteps(this.userData)
							.subscribe(
								(data: any) => {
									if (data.success == 0) {
										this.errorCheck = true;
										this.errorMessage = data.msg;
									} else {
										console.log(data);

										data.user_info.user_id = data.user_info.id;
										delete data.user_info.id;
										let newuser_data = JSON.stringify(data.user_info);
										localStorage.setItem('regForm.u_data', newuser_data);
										localStorage.setItem('regForm.step', '1');
										//localStorage.setItem('regForm.name',this.regdata.name);
										//localStorage.setItem('regForm.email',this.regdata.email);
										this.router.navigate(['signup1']);

									}
								});
					}
					else {
						this.errorCheck = true;
						this.errorMessage = "Please add email id in twitter and give permission JIVIN to access it.";
					}
				}
				else {
					this.errorCheck = true;
					this.errorMessage = "Something went wrong. Please try again.";
				}
			}, error => {
				this.errorCheck = true;
				this.errorMessage = "Something went wrong. Please try again.";
			});
	}

	linkedinAuth(code: any) {
		this.social_media_service.get_linkedin_accesstoken(code, this.client_id, this.client_secret, 0)
			.subscribe((data) => {
				console.log(data);
				if (data.success && data.data.access_token) {
					var access_token = data.data.access_token;
					//get user profile
					this.social_media_service.get_user_linkedin_profile(access_token)
						.subscribe((data) => {

							if (data.success) {
								this.userData.name = data.data.firstName.localized.en_US + ' ' + data.data.lastName.localized.en_US;
								console.log(this.userData.name);
								this.userData.social_uid = data.data.id;
								let profile_image_data = data.data.profilePicture["displayImage~"]['elements'];
								console.log(profile_image_data[profile_image_data.length - 1].identifiers[0].identifier);
								localStorage.setItem('linkedin_profile_image', profile_image_data[profile_image_data.length - 1].identifiers[0].identifier);
								console.log(localStorage.getItem('linkedin_profile_image'));
								//get email
								this.social_media_service.get_user_linkedin_email(access_token)
									.subscribe((data) => {
										console.log(data);
										if (data.success) {
											this.userData.email = data.data.elements[0]['handle~']['emailAddress'];
											this.userData.registrationtype = 'linkedin';
											this.social_media_service.regSteps(this.userData)
												.subscribe(
													(data: any) => {
														if (data.success == 0) {
															this.errorCheck = true;
															this.errorMessage = data.msg;
														} else {
															console.log(data);

															data.user_info.user_id = data.user_info.id;
															delete data.user_info.id;
															let newuser_data = JSON.stringify(data.user_info);
															localStorage.setItem('regForm.u_data', newuser_data);
															localStorage.setItem('regForm.step', '1');
															//localStorage.setItem('regForm.name',this.regdata.name);
															//localStorage.setItem('regForm.email',this.regdata.email);
															this.router.navigate(['signup/step1']);

														}
													});

										}
										else {
											console.log(data.msg);
										}
									}, error => {
										console.log('Error Occurred In API.');
									});



							}
							else {
								console.log(data.msg);
							}


						}, error => {
							console.log('Error Occurred In API.');
						});
				}
				else {
					console.log(data.msg);
				}
			}, error => {
				console.log('Error Occurred In API.');
			})
	}

	instagramAuth(code: any) {
		this.social_media_service.get_instagram_accesstoken(code, this.instagram_client_id, this.instagram_client_secret, 0)
			.subscribe((data) => {
				console.log(data);
				if (data.success && data.data.access_token) {
					var access_token = data.data.access_token;
					this.userData.name = data.data.user.full_name;

					this.userData.social_uid = data.data.user.id;
					let profile_image_data = data.data.user.profile_picture;

					localStorage.setItem('instagram_profile_image', profile_image_data);

					this.userData.email = data.data.user.username;
					this.userData.username = data.data.user.username;
					localStorage.setItem('user_name', data.data.user.username);
					this.userData.registrationtype = 'instagram';
					this.social_media_service.regSteps(this.userData)
						.subscribe(
							(data: any) => {
								if (data.success == 0) {
									this.errorCheck = true;
									this.errorMessage = data.msg;
								} else {
									console.log(data);

									data.user_info.user_id = data.user_info.id;
									delete data.user_info.id;
									let newuser_data = JSON.stringify(data.user_info);
									localStorage.setItem('regForm.u_data', newuser_data);
									localStorage.setItem('regForm.step', '1');
									//localStorage.setItem('regForm.name',this.regdata.name);
									//localStorage.setItem('regForm.email',this.regdata.email);
									this.router.navigate(['signup/step1']);

								}
							});

				}
				else {
					console.log(data.msg);
				}
			}, error => {
				console.log('Error Occurred In API.');
			})
	}

	facebookAuth() {
		const options: LoginOptions = {
			scope: 'public_profile,user_friends,email,pages_show_list',
			return_scopes: true,
			enable_profile_selector: true
		};
		this.facebook.login(options)
			.then((response: LoginResponse) => {
				console.log(JSON.stringify(response));
				this.facebook.api('/me?fields=id,name,email')
					.then(res => {
						console.log(res);
						if (res.email) {
							this.userData.email = res.email;
							this.userData.name = res.name;

							localStorage.setItem('fb_uid', res.id);
							this.userData.social_uid = res.id;
							this.userData.registrationtype = 'facebook';
							this.social_media_service.regSteps(this.userData)
								.subscribe(
									(data: any) => {
										if (data.success == 0) {
											this.errorCheck = true;
											this.errorMessage = data.msg;
										} else {
											console.log(data);

											data.user_info.user_id = data.user_info.id;
											delete data.user_info.id;
											let newuser_data = JSON.stringify(data.user_info);
											localStorage.setItem('regForm.u_data', newuser_data);
											localStorage.setItem('regForm.step', '1');
											//localStorage.setItem('regForm.name',this.regdata.name);
											//localStorage.setItem('regForm.email',this.regdata.email);
											this.router.navigate(['signup/step1']);

										}
									});
						}
						else {
							this.errorCheck = true;
							this.errorMessage = "Email id is necessary to signup with facebook. Please allow permission to access your facebook email.";
						}

					})
					.catch(e => console.log(e));
			})
			.catch((error: any) => console.error(error));

	}

	ngOnInit() {
	}

	ngOnDestroy() {
		// this.sub.unsubscribe();
	}

	ngAfterViewChecked() {
		this.formChanged();
	}

	formChanged() {
		if (this.currentForm === this.regForm) { return; }
		this.regForm = this.currentForm;
		if (this.regForm) {
			this.regForm.valueChanges
				.subscribe(data => this.onValueChanged(data));
		}
	}

	onValueChanged(data?: any) {
		if (!this.regForm) { return; }
		const form = this.regForm.form;

		for (const field in this.formErrors) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);

			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field] += messages[key] + ' ';
				}
			}
		}
	}

	formReg() {
		if (this.regForm.form.valid) {
			this.userData.email = this.regdata.email;
			this.userData.name = this.regdata.name;
			//console.log(this.userData);
			this.http.post(DevConfig.API + 'api/v1/register', JSON.stringify(this.userData))
				.subscribe((data) => {
					this.api_response = {};
					this.api_response = data.json();
					if (this.api_response && this.api_response.success) {
						this.api_response.data.user_info.user_id = this.api_response.data.user_info.id;
						let newuser_data = JSON.stringify(this.api_response.data.user_info);
						localStorage.setItem('regForm.u_data', newuser_data);
						localStorage.setItem('regForm.step', '1');
						this.router.navigate(['signup1']);

					}
					else {
						alert("Something went wrong. Try Again!");
					}
				})


			//this.router.navigate(['signup/step1']);
		} else {

			this.errorCheck = true;
			this.errorMessage = "Error in Submission";
		}

		setTimeout(() => {
			this.errorCheck = false;
			this.errorMessage = "Error in Submission";
		}, 5000);

	}

	signIn(provider: any) {
		console.log(provider);


		if (provider == "linkedin") {
			var options = '';
			options += ',width=600px';
			options += ',height=600px';

			window.open("https://www.jivin.com/development/jivin/services/public/linkedin_login", "LinkedIn Authentication", options);
		}
		else if (provider == "facebook") {
			this.facebookAuth();
		}
		else if (provider == "instagram") {
			var options = 'left=200px';
			options += ',width=600px';
			options += ',height=600px';

			window.open("https://www.jivin.com/development/jivin/services/public/instagram_login", "Instagram Authentication", options);
		}
		else if (provider == "twitter") {
			var options = 'left=200px';
			options += ',width=600px';
			options += ',height=600px';

			window.open("https://www.jivin.com/development/jivin/services/public/api/v1/get_access_token_twitter", "Twitter Authentication", options);
		}
		else {
			var new_this = this;
			this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(function (data) {

				console.log(data);
			});
		}
	}

}
