import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidepanelTopComponent } from './sidepanel-top.component';

describe('SidepanelTopComponent', () => {
  let component: SidepanelTopComponent;
  let fixture: ComponentFixture<SidepanelTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidepanelTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidepanelTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
