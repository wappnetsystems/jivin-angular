import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
declare var $: any
@Component({
  selector: 'app-sidepanel-top',
  templateUrl: './sidepanel-top.component.html',
  styleUrls: ['./sidepanel-top.component.css']
})
export class SidepanelTopComponent implements OnInit {

  userInformation: any;
	backgroundImage: any;
	profileImg: any;
  user_type:string;
  
  constructor(private router: Router) {
    this.userInformation = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
    if (this.userInformation.user_info.account_settings.background_image_url) {
			this.backgroundImage = "url('" + this.userInformation.user_info.account_settings.background_image_url + "')";
		} else {
			this.backgroundImage = "url('assets/img/images/blue.jpg')";
    }
    this.user_type=this.userInformation.user_info.user_type;
		
		//console.log("int",this.userInterests);
		this.profileImg = this.userInformation.user_info.user_image_url;
  }

}
