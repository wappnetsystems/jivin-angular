import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { DevConfig } from './env.config';
@Injectable({
  providedIn: 'root'
})
export class CommonApiService {

  constructor(private http: Http) { }

  get_total_video_votes(api_key: any, user_id: number) {
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    headers.append('X-Authorization', api_key);

    let options = new RequestOptions({ headers: headers });
    let data = { 'user_id': user_id };
    return this.http.post(DevConfig.API + 'api/v1/get_total_video_votes', data, options)
      .map((response: Response) => {
        return response.json();
      });
  }

  get_total_audio_votes(api_key: any, user_id: number) {
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    headers.append('X-Authorization', api_key);

    let options = new RequestOptions({ headers: headers });
    let data = { 'user_id': user_id };
    return this.http.post(DevConfig.API + 'api/v1/get_total_audio_votes', data, options)
      .map((response: Response) => {
        return response.json();
      });
  }

  getFollowersList(data: any, api_key: any) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Authorization', api_key);

    let options = new RequestOptions({ headers: headers });

    let acc_data = JSON.stringify(data);
    //console.log(api_key);
    return this.http.post(DevConfig.API + 'api/v1/get-following-user-list', acc_data, options)
      .map((response: Response) => {


        let result = response.json();
        if (result && result.success == 1) {
          return result.data;
        } else {
          return 0;
        }
      });
  }

  getAdimage(data: any, api_key: string) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Authorization', api_key);

    let options = new RequestOptions({ headers: headers });

    let acc_data = JSON.stringify(data);


    return this.http.post(DevConfig.API + '/api/v1/showimageadd', acc_data, options)
      .map((response: Response) => {
        let result = response.json();
        if (result && result.success == 1) {
          return result.data;
        } else {
          return false;
        }
      });

  }

  getProfileDetails(data: any) {

    let json_data = JSON.stringify(data);

    return this.http.post(DevConfig.API + '/api/v1/get_profile_details', json_data)

      .map((response: Response) => {

        let result = response.json();

        if (result && result.success == 1) {

          return result.data;
        } else {
          return false;
        }
      });
  }

  getCurrentBoostedLocation(data: any) {

    let json_data = JSON.stringify(data);
    return this.http.post(DevConfig.API + '/api/v1/getcurrentboostedlocation', json_data)
        .map((response: Response) => {
            let result = response.json();
            if (result && result.success == 1) {
                return result.data;
            } else {
                return false;
            }
        });
}

getsitesetting() {
  return this.http.get(DevConfig.API + '/api/v1/sitesetiing')
      .map((response: Response) => {
          let result = response.json();
          if (result && result.success == 1) {
              return result.data;
          } else {
              return false;
          }
      });
}

  deletepost(data: any) {
    let json_data = JSON.stringify(data);
    return this.http.post(DevConfig.API + '/api/v1/deleteyourpost', json_data)
        .map((response: Response) => {
            let result = response.json();
            //if (result && result.success==1) {
            return result;
            // }else{
            //     return false;
            // }
        });
}

  get_video_post_by_user_id(user_id: number, api_key: any) {
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    headers.append('X-Authorization', api_key);
    let options = new RequestOptions({ headers: headers });

    let data = { 'user_id': user_id };
    return this.http.post(DevConfig.API + 'api/v1/get_video_post_by_user_id', data, options)
      .map((response: Response) => {
        return response.json();
      });
  }

  get_audio_post_by_user_id(user_id: number, api_key: any) {
    let headers = new Headers();
    headers.append('Content-type', 'application/json');
    headers.append('X-Authorization', api_key);
    let options = new RequestOptions({ headers: headers });

    let data = { 'user_id': user_id };
    return this.http.post(DevConfig.API + 'api/v1/get_audio_post_by_user_id', data, options)
      .map((response: Response) => {
        return response.json();
      });
  }

  getAllPost(data: Object, api_key: any) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Authorization', api_key);

    let options = new RequestOptions({ headers: headers });

    let postData = JSON.stringify(data);
    //console.log(regiData);
    return this.http.post(DevConfig.API + 'api/v1/get_posts', postData, options)
      .map((response: Response) => {

        let result = response.json();

        if (result && result.success == 1) {
          return result.data;

        } else {
          return false;
        }
      });

  }

  getAllCountry() {

    return this.http.get(DevConfig.API + '/api/v1/get-country-list', '')
        .map((response: Response) => {
            // login successful if there's a jwt token in the response
            let result = response.json();
            //console.log(JSON.stringify(result));
            //console.log(result.success);
            if (result && result.success == 1) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('currentUser', JSON.stringify(result));
                return result.data;
            } else {

                //	alert("Opps! Please try after some time!");
            }
        });
}

geTotalMedia(data: any, api_key: string) {
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('X-Authorization', api_key);

  let options = new RequestOptions({ headers: headers });
  let json_data = JSON.stringify(data);

  return this.http.post(DevConfig.API + 'api/v1/total_media_count', json_data, options).map((response: Response) => {
      return response.json();
  });
}

changepostpermission(data: any) {
  let json_data = JSON.stringify(data);
  return this.http.post(DevConfig.API + '/api/v1/changepostpermission', json_data)
      .map((response: Response) => {
          let result = response.json();
          //if (result && result.success==1) {
          return result;
          // }else{
          //     return false;
          // }
      });
}

boostpost(data: any) {
  let json_data = JSON.stringify(data);
  return this.http.post(DevConfig.API + '/api/v1/postyourboost', json_data)
      .map((response: Response) => {
          let result = response.json();
          //if (result && result.success==1) {
          return result;
          // }else{
          //     return false;
          // }
      });
}

}
