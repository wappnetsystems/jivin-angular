import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { DevConfig } from './env.config';
@Injectable({
  providedIn: 'root'
})
export class SocialMediaService {

  constructor(private http:Http) { 

  }
  get_user_twitter_profile(temp_id: string) {
    let headers=new Headers();
    headers.append('Content-Type','application/json');
    let token_data={'temp_id':temp_id};

    let options=new RequestOptions({headers:headers});

    return this.http.post(DevConfig.API+'api/v1/get_user_profile_twitter',token_data,options)
            .map((response:Response)=>{
              return response.json();
            });

  }
  regSteps(data: Object) {

    let json_data = JSON.stringify(data);

    //console.log(json_data);
    return this.http.post(DevConfig.API + 'api/v1/register', json_data)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let result = response.json();
        
        if (result && result.success == 1) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          
          return result.data;
        } else {

          //alert("Opps! Please try after some time!");
          return result;
        }
      });
  }
  get_linkedin_accesstoken(code: any, client_id: string, client_secret: string,flag:any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let linkedin_data = { 'flag':flag,'grant_type': 'authorization_code', 'code': code, 'client_id': client_id, 'client_secret': client_secret };
    return this.http.post(DevConfig.API + 'api/v1/get_access_token_linkedin', linkedin_data, options)
      .map((response: Response) => {
        return response.json();
      });

  }
  get_user_linkedin_profile(access_token: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let token_data = { 'access_token': access_token };
    let options = new RequestOptions({ headers: headers });
    return this.http.post(DevConfig.API + 'api/v1/get_user_profile_linkedin', token_data, options)
      .map((response: Response) => {
        return response.json();
      });

  }
  get_user_linkedin_email(access_token: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let token_data = { 'access_token': access_token };
    let options = new RequestOptions({ headers: headers });
    return this.http.post(DevConfig.API + 'api/v1/get_user_email_linkedin', token_data, options)
      .map((response: Response) => {
        return response.json();
      });

  }

  get_instagram_accesstoken(code: any, client_id: string, client_secret: string,flag:any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let instagram_data = { 'flag':flag,'grant_type': 'authorization_code', 'code': code, 'client_id': client_id, 'client_secret': client_secret };
    return this.http.post(DevConfig.API + 'api/v1/get_access_token_instagram', instagram_data, options)
      .map((response: Response) => {
        return response.json();
      });

  }

  get_user_instagram_profile(access_token: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let token_data = { 'access_token': access_token };
    let options = new RequestOptions({ headers: headers });
    return this.http.post(DevConfig.API + 'api/v1/get_user_profile_instagram', token_data, options)
      .map((response: Response) => {
        return response.json();
      });

  }
}
