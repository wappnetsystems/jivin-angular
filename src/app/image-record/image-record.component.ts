import { Component, OnInit,Input,Output,AfterViewInit,EventEmitter } from '@angular/core';
declare var videojs: any;
@Component({
  selector: 'app-image-record',
  templateUrl: './image-record.component.html',
  styleUrls: ['./image-record.component.css']
})
export class ImageRecordComponent implements OnInit,AfterViewInit {
  @Input() public width: number;
  @Input() public height: number;
  @Input() public maxlength: number;

  @Output() onDeviceError = new EventEmitter();
  @Output() onFinishRecordc = new EventEmitter();

  public randomId: string;

  private player: any;


  constructor() {
    this.randomId = 'myVideo' + String(new Date().getTime());
   }

  ngOnInit() {
  }

  ngAfterViewInit() {



    this.player =   videojs(this.randomId,
  {
      controls: false,
      width: 320,
      height: 240,
      controlBar: {
          volumeMenuButton: false,
          fullscreenToggle: false
      },
      plugins: {
          record: {
              image: true,
              debug: true
          }
      }
  });
  
      this.player.on('deviceError', () => {
        //this.onDeviceError.emit({ message: 'Device Error', code: this.player.deviceErrorCode });
            console.log('device error:', this.player.deviceErrorCode);
            console.log('device error:', this.player.deviceErrorCode.name);
            if(this.player.deviceErrorCode.name === 'DevicesNotFoundError') {
              alert('No device connected. Please connect a device.');  
            }
      });
  
  
  
      this.player.on('finishRecord', () => {
        
      //  console.log(this.player.recordedData);
        this.onFinishRecordc.emit({ message: 'Finish Record', data: this.player.recordedData });
      });
    }

}
