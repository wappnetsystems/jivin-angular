import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { WallComponent } from './wall/wall.component';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { FacebookModule } from 'ngx-facebook';
import { LoginFooterComponent } from './login-footer/login-footer.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { AdvertisingComponent } from './advertising/advertising.component';
import { AdChoicesComponent } from './ad-choices/ad-choices.component';
import { CookiesComponent } from './cookies/cookies.component';
import { LanguagesComponent } from './languages/languages.component';
import { Signupstep1Component } from './signupstep1/signupstep1.component';
import { Signupstep2Component } from './signupstep2/signupstep2.component';
import { Signupstep3Component } from './signupstep3/signupstep3.component';
import { Signupstep3MapComponent } from './signupstep3-map/signupstep3-map.component';
import { Signupstep4Component } from './signupstep4/signupstep4.component';
import { ImageRecordComponent } from './image-record/image-record.component';
import { WebcamModule } from 'ngx-webcam';
import { Signupstep5Component } from './signupstep5/signupstep5.component';
import { Signupstep6Component } from './signupstep6/signupstep6.component';
//import { CeiboShare } from 'ng2-social-share';
import { Signupstep7Component } from './signupstep7/signupstep7.component';
import { RegisterComponent } from './register/register.component';
import { NgxSocialShareModule } from 'ngx-social-share';
import { SidepanelComponent } from './sidepanel/sidepanel.component';
import { SidepanelTopComponent } from './sidepanel-top/sidepanel-top.component';
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("172760255594-p355fqrcq2pauimc7vqo8ngqbsmb98j3.apps.googleusercontent.com")
  },

]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WallComponent,
    LoginFooterComponent,
    PrivacyComponent,
    TermsComponent,
    AdvertisingComponent,
    AdChoicesComponent,
    CookiesComponent,
    LanguagesComponent,
    Signupstep1Component,
    Signupstep2Component,
    Signupstep3Component,
    Signupstep3MapComponent,
    
    Signupstep4Component,
    
    ImageRecordComponent,
    
    Signupstep5Component,
    
    Signupstep6Component,
    //CeiboShare,
    Signupstep7Component,
    RegisterComponent,
    SidepanelComponent,
    SidepanelTopComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SocialLoginModule,
    FacebookModule.forRoot(),
    WebcamModule,
    NgxSocialShareModule,
    
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
