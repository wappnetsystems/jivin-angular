import { Component, OnInit, Renderer, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DevConfig } from '../env.config';
declare var $: any
@Component({
	selector: 'app-signupstep3',
	templateUrl: './signupstep3.component.html',
	styleUrls: ['./signupstep3.component.css']
})
export class Signupstep3Component implements OnInit, OnDestroy {

	constructor(private renderer: Renderer, private router: Router, private http: Http) {
		this.renderer.setElementClass(document.body, 'register_bg', true);
	}
	api_response: any = {};
	suggested_followers: Object;
	clickedFollower: any = [];
	finalFollower: string = '';
	totalFollower: any = 0;
	followAll: any = false;
	countFollowers: any;
	suggested_followers_ids: any;
	logData: any;
	userData: any = {
		user_id: 0,
		finish_reg: 0,
		name: "",
		email: "",
		secondary_email: "",
		username: "",
		birth_month: "",
		birth_year: "",
		password: "",
		user_interests: "",
		is_following: "",
		country_id: "",
		region_id: "",
		division_id: "",
		state_id: ""

	}

	userInformation: any = {};

	ngOnInit() {
		this.logData = JSON.parse(localStorage.getItem('regForm.u_data'));
		this.userData.name = this.logData.name;
		this.userData.email = this.logData.email;
		this.userData.user_id = localStorage.getItem('regForm.t');
		this.userData.user_interests = localStorage.getItem('regForm.int');
		this.userData.finish_reg = 0;

		this.http.post(DevConfig.API + 'api/v1/register', JSON.stringify(this.userData))
			.subscribe((data) => {
				this.api_response = data.json();
				if (this.api_response.success) {
					this.suggested_followers = this.api_response.data.suggested_followers;
					this.countFollowers = this.api_response.data.suggested_followers.length;
					this.suggested_followers_ids = this.api_response.data.suggested_followers_ids;
				}
				else {
					alert('Something went wrong. No suggesstions found!');

				}

			}, error => {
				alert('Something went wrong. No suggesstions found!');

			});
		let newuser_data_m = JSON.stringify(this.userData);
		localStorage.setItem('regForm.u_data', newuser_data_m);
		//localStorage.removeItem('regForm.int');
		localStorage.setItem('regForm.step', '4');

	}
	ngOnDestroy() {
		this.renderer.setElementClass(document.body, 'register_bg', false);
	}

	addFollower(value: any) {
		//console.log(value);

		if (value == "") {
			alert("Please follow to continue!");
		} else {

			$("#" + value + "_fol").remove();
			this.clickedFollower.push(value);

			this.finalFollower = this.clickedFollower.join(',');
			this.totalFollower = this.totalFollower + 1;

			if (this.totalFollower == this.countFollowers) {
				this.followAll = true;
			}

			localStorage.setItem('regForm.follow', this.finalFollower);

		}


	}

	followEveryOne() {

		localStorage.setItem('regForm.follow', this.suggested_followers_ids);
		this.router.navigate(['/signup3-map']);
	}

	followParticular() {

		localStorage.setItem('regForm.follow', this.finalFollower);
		this.router.navigate(['/signup3-map']);
	}

}
