import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep3Component } from './signupstep3.component';

describe('Signupstep3Component', () => {
  let component: Signupstep3Component;
  let fixture: ComponentFixture<Signupstep3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
