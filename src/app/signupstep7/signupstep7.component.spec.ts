import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep7Component } from './signupstep7.component';

describe('Signupstep7Component', () => {
  let component: Signupstep7Component;
  let fixture: ComponentFixture<Signupstep7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
