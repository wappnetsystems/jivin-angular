import { Component, OnInit, Renderer, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DevConfig } from '../env.config';
import { Http } from '@angular/http';
@Component({
  selector: 'app-signupstep7',
  templateUrl: './signupstep7.component.html',
  styleUrls: ['./signupstep7.component.css']
})
export class Signupstep7Component implements OnInit,OnDestroy {

  constructor(private router: Router,
    private route: ActivatedRoute, private renderer: Renderer,private http:Http) {
    this.renderer.setElementClass(document.body, 'register_bg', true);
    let step_calculator = localStorage.getItem('regForm.step');

		/*if(step_calculator!='7') {
		    this._location.back();
		}*/
    if (localStorage.getItem("currentUser")) {
      this.router.navigate(['wall']);
      return;
    }
  }
  model: any = {};
	returnUrl:string;
	ngOnDestroy(){
		this.renderer.setElementClass(document.body, 'register_bg', false);
	}
  ngOnInit() {
    localStorage.removeItem('currentUser');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/wall';

    this.model = JSON.parse(localStorage.getItem('regForm.u_data'));
    localStorage.removeItem('regForm.email');
		localStorage.removeItem('regForm.name');
		localStorage.removeItem('regForm.u_data');
		localStorage.removeItem('regForm.follow');
    localStorage.removeItem('regForm.int');
    this.http.post(DevConfig.API + 'api/v1/login', JSON.stringify({ username: this.model.username, password: this.model.password, logintype: '' }))
              .subscribe((data)=>{
                let user=data.json();
                if (user && user.success == 1 && user.data.api_key) {
                  localStorage.setItem('currentUser', JSON.stringify(user.data));
                  setTimeout((router: Router) => {
                    this.router.navigate([this.returnUrl]);
                  }, 4000);
                }
                else{
                  alert("We are very sorry! Failed to setup your profile!");
                  this.router.navigate([this.returnUrl]);
                }
              },error=>{
                alert("We are very sorry! Failed to setup your profile!");
                this.router.navigate([this.returnUrl]);
              })
  }

}
