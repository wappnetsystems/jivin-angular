import { Component, OnInit,Renderer,OnDestroy } from '@angular/core';
import { Http,Headers,RequestOptions} from '@angular/http';
import { DevConfig } from '../env.config';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signupstep2',
  templateUrl: './signupstep2.component.html',
  styleUrls: ['./signupstep2.component.css']
})
export class Signupstep2Component implements OnInit,OnDestroy {

  constructor(private renderer:Renderer,private http:Http,private router:Router) { 
    this.renderer.setElementClass(document.body, 'register_bg', true);
    let step_calculator = localStorage.getItem('regForm.step');
  }

  interestData:Object;
	checkedInst:any = [];
	finalChk:string = '';
	errorCheck : boolean = true ;
  api_response:any={};
  ngOnInit() {
    return this.http.get(DevConfig.API + '/api/v1/getallinterestcategory', '')
                    .subscribe((data)=>{
                      
                      this.api_response = data.json();
                      if(this.api_response.success){
                        this.interestData=this.api_response.data;
                      }
                      else{
                        alert('Something went wrong. Try Again!');
                      }
                    },error=>{
                      alert('Something went wrong. Try Again!');
                    });
  }

  ngOnDestroy(){
    this.renderer.setElementClass(document.body,'register_bg',false);
  }

  updateCheckBox(value:any, event:any){
    if(event.target.checked) {			
			this.errorCheck = true;
			this.checkedInst.push(value);
		}else{
			this.checkedInst.splice(this.checkedInst.indexOf(value),1);
		}

		this.finalChk = this.checkedInst.join(',');
		localStorage.setItem('regForm.int', this.finalChk);
  }

  submitForm(value:any){

		if(value=="all") {

			localStorage.setItem('regForm.int', "all");
			this.router.navigate(['signup3']);

		}else{

			if(this.finalChk=="") {
				this.errorCheck = false;
			}else{
				this.router.navigate(['signup3']);
			}
		}

		setTimeout(() => {
			this.errorCheck = true;
		}, 5000);
		
	}

}
