import { TestBed } from '@angular/core/testing';

import { AsyncdataService } from './asyncdata.service';

describe('AsyncdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsyncdataService = TestBed.get(AsyncdataService);
    expect(service).toBeTruthy();
  });
});
