import { Component, ViewChild, ElementRef, AfterViewInit, NgModule, Input, AfterViewChecked, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { AsyncdataService } from '../asyncdata.service';
import { CommonApiService } from '../common-api.service';
import { DevConfig } from '../env.config';

declare var $: any;
declare var WaveSurfer: any;
declare var bootbox: any;

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.css']
})
export class WallComponent implements OnInit, OnDestroy, AfterViewChecked {

  location_type: string;
	boosttext: string;
	compitition: any;
	filtername: any;
	dataExists: boolean;
	leadImgUrl: any;
	leaderObj: any;
	loader: boolean;
	crurrntBoosted:any;
	showDropdown:boolean = false;
	siteSetting: any = [];
	baseUrl: string;
	disabled = false;

	delete_post_data:any;
	delete_post_id:number;
  userInformation:any;
  constructor(private router:Router, private commonService: CommonApiService, 
    private asyncdataService : AsyncdataService, private element: ElementRef) { 
    
    if(!localStorage.getItem('currentUser')){
      this.router.navigate(['']);
    }

    this.userInformation = JSON.parse(localStorage.getItem('currentUser'));
		this.siteSetting = JSON.parse(localStorage.getItem('sitesetting'));
		if (!this.userInformation) {
			this.router.navigate(['/']);
		}
		this.baseUrl = DevConfig.BASE_URL;

  }

  public submitted: boolean = false;
	public submiting_value: boolean = false;
	public wavesurferObj: any;

	private _reportData: any = {};

	//Post data initialization

	postData: any = {};
	postForm: NgForm;
	@ViewChild('postForm',{static:true}) currentForm1: NgForm;

	allPost: any = [];

	//end of post data initialization


	eventF: NgForm;
	@ViewChild('eventF',{static:true}) currentForm: NgForm;

	// Event Data Initialization 
	eventInfo: any = {};

	eventData: any = {};

	successData: any;
	loading = false;
	errorMessage = false;
	successMessage = false;


	formErrors: any = {
		'name': '', 'power': ''
	};

	validationMessages: any = {};

	// end of event data initialization

	countryData: any;
	regionData: any;
	divisionData: any;
	stateData: any;
	valid_date_eve: boolean = false;
	valid_time_eve: boolean = false;
	dialog: any;
	/*Music palyer var*/
	musicPlayer: any = "";
	currentTime: string = "00:00";
	totalTime: string = "--:--";
	element_div: string = "";
	prev_index: any = "";

	sendingObjPost: any = {};
	paginationCall: boolean = false;
	reportValidation = false;
	success =false;
	perpagecount:any;
	totalpage:any;
	pagCount:any;
	private _tableLoader =false;
	private noFound=false;
	user_type:string='';
	is_compitator:boolean=true;
	profile_image:string="";

  ngOnInit() {
    this.profile_image = this.userInformation.user_info.user_image_url;
		this.asyncdataService.getLogo().subscribe((data) => {
			
			this.profile_image = data;
			
		});

		this.user_type=this.userInformation.user_info.user_type;
		if(this.user_type=="Fan"){
			
			this.is_compitator=false;
		}
		else{
			this.is_compitator=true;
		}
		this.sendingObjPost = { user_id: this.userInformation.user_info.id, user_state:this.userInformation.user_info.account_settings.state.slug, current_user_id: this.userInformation.user_info.id, page: 1, itemsPerPage: 10 };
		//================infinte scroll===================//
		var win = $(window);
		win.scroll(function () {
			if ($(document).height() - win.height() == win.scrollTop()) {

				console.log(1);
			}
		})
		//===================================//
		localStorage.removeItem('f_n_v');
		localStorage.removeItem('f_n_a');

		$("#loadingPopup").modal({
			"show": true,       // ensure the modal is shown immediately
		});

		this.getAllPostToWall();// call for showing all post to the wall

		this.commonService.getAllCountry()  //call for get all country lists
			.subscribe(
			(data: Object) => {
				this.countryData = data;
			},
			error => { }
			);
		
		this.eventData.selectcountry = "";
		this.eventData.selectregion = "";
		this.eventData.selectdivision = "";
		this.eventData.selectstate = "";

		if (this.userInformation.user_info.account_settings.background_color_code) {
			let color_cd = this.userInformation.user_info.account_settings.background_color_code;
			$('body').css("background", color_cd);
		}

		/*if(this.userInformation.user_info.account_settings.background_image_url) {
			let src = this.userInformation.user_info.account_settings.background_image_url;
			$('body').css("background-image", "url("+src+")");
		}*/
		console.log(this.userInformation);
		this._getTotalMedia();

		$('.modal-title .fa').click(function(){
			$('.poplargtxt').slideToggle();
		});
  }

  ngOnDestroy() {
		$('#loadingPopup').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();

		if( this.wavesurferObj){
			this.wavesurferObj.destroy();
		}
	}
	openpermission(drop_id:any){
		let drop_down_id = "#drop_list_"+drop_id;
		$(drop_down_id).slideToggle();
	}

  ngAfterViewChecked() {
		this.formChanged();
		let $this = this;
		$(".report-post").click(function () {
			$this._reportData.post_id = $(this).data('postid');
			console.log($(this).data('postId'));
			$("#reportProblem").modal();
			$this._reportData.reportValidation = false;
			$this._reportData.success =false;
		});

		//
		$('#myModalevent').on('hidden.bs.modal', function () {
			// do something…
			$this.submitted = false;
			$this.eventF.form.reset();
		});
  }
  
  boostmodal(post_id:any,user_id:any){
		/*
		let dd_obj = { country_slug:"US" };
		this.commonService.getAllRegion(dd_obj, 'regions')
    		.subscribe(

    			(data:Object) => {
    				this.regionData = data;
    			},
    			error => { }
    		);
		
		$("#boost_post_id").val(post_id);
		$("#boost_location").val("US");
		
		$("#boost_location_type").val("country");
		this.location_type = "Country";
		this.getbooscheck(this.location_type);
		*/
		/***New Code for boost start ***/
		$("#boost_post_id").val(post_id);
		$("#boost_location").val("US");
		let data = {"userid": user_id};
		this.commonService.getCurrentBoostedLocation(data)
			.subscribe(
			(data: any) => {
				if (data) {
					this.crurrntBoosted = data;
					this.showDropdown = true;
					console.log(this.crurrntBoosted);
				}
			},
			error => {});
		this.commonService.getsitesetting().subscribe(
			(data: any) => {
				this.siteSetting = data;
				localStorage.setItem('sitesetting', JSON.stringify(data));
			},
			error => {});


		$("#boost_location_type").val("country");
		this.location_type = "Country";
		this.getbooscheck(this.location_type);
		/***New Code for boost end ***/
		$("#boostmodal").modal();
	}

  getAllPostToWall() {
	
		this.commonService.getAllPost(this.sendingObjPost, this.userInformation.api_key)
			.subscribe(
			(data: any) => {
				// this.allPost = [];
				// console.log("Dhiman Wall");
				// console.log(this.userInformation.user_info.id);
				if (data != false) {
					

					this.allPost = this.allPost&&this.allPost.length>0?this.allPost.concat(data[0].data):data[0].data;
					//this.allPost = data[0].data;
					// 	for (var i = 0; i < this.allPost.length; i++) {
					// 	this.allPost[i].reportData = {};
					// }
					this.perpagecount =  data[0].current_page;
					this.totalpage = 	 data[0].last_page;
					
	            	this._tableLoader=false;

					//this.sendingObjPost.page = this.sendingObjPost.page + 1;
					//this.paginationCall = true;
				} else {
					this.allPost = [];
					//this.paginationCall = false;
				}
				console.log(data[0]);
			},
			error => { },
			() => {

				$("#loadingPopup").modal('hide');
			}
			);
  }

  formChanged() {

		if (this.currentForm === this.eventF) { return; }
		this.eventF = this.currentForm;
		if (this.eventF) {
			this.eventF.valueChanges
				.subscribe(data => this.onValueChanged(data));
		}
  }
  
  onValueChanged(data?: any) {

		if (!this.eventF) { return; }
		const form = this.eventF.form;

		for (const field in this.formErrors) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);

			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field] += messages[key] + ' ';
				}
			}
		}
  }
  
  changepermission(drop_id:any,userid:any,postid:any,status:any){

		//alert(userid+"******"+postid+"*******"+status);
		// console.log(drop_id);
		let drop_down_id = "#drop_list_"+drop_id.id;;
		let senddata = {"post_id":postid,"user_id":userid,"permission":status};
				this.commonService.changepostpermission(senddata)
				.subscribe(
				(data: any) => {
					$(drop_down_id).slideToggle();
					console.log(data);					
				if(data.success == 1){
					if( status === 3){
						drop_id.is_public = 0;
					} else {
						drop_id.is_public = status;
					}
					// console.log(status);
					// console.log(drop_id);
					// drop_id.is_public = status;
					$("#boostsuccess"+postid).html(data.msg);
					$("#boostsuccess"+postid).addClass("showhide");
					setTimeout(function(){ $("#boostsuccess"+postid).removeClass("showhide"); }, 3000);
				}else{
					$("#boosterror"+postid).html(data.msg);
					$("#boosterror"+postid).addClass("showhide");
					setTimeout(function(){ $("#boosterror"+postid).removeClass("showhide"); }, 3000);
				}
					
				},
				error => {
					//this.alertService.error(error);
					//this.errorCheck = false;
				}
			);

  }
  
  playMusic(audio_url: string, div_id: string, index: any) {

		if (this.musicPlayer) {

			this.musicPlayer.empty();
			$('#' + this.element_div + ' > wave').remove();
			$('#waveform_' + this.prev_index + ' > img').show();
			$('#play_pause_button_' + this.prev_index).hide();
			$('#play_button_' + this.prev_index).show();
			$('#scrubber_' + this.prev_index + ' > .progress').css('width', "0%");
			$('#current_time_' + this.prev_index).html('00:00');
			$('#audiojs_wrapper_' + this.prev_index + ' > .play-pause').removeClass('playing');
		}

		this.prev_index = index;
		this.element_div = div_id;

		const wavesurfer = WaveSurfer.create({
			container: '#' + div_id,
			waveColor: 'grey',
			progressColor: 'darkorange',
			scrollParent: false,
			barWidth: 1,
			barHeight: 2,
			//cursorColor:'#000',
			//autoCenter:true,
			//maxCanvasWidth:500
		});
		this.wavesurferObj = wavesurfer;
		$('#waveform_' + index + ' > wave').hide();
		//wavesurfer.load('/assets/Bade_Acche_Laggte_Hai.mp3');
		//wavesurfer.load('/assets/example.aac');
		wavesurfer.load(audio_url);

		wavesurfer.on('ready', function () {

			$('#waveform_' + index + ' > img').hide();
			$('#waveform_' + index + ' > wave').show();
			wavesurfer.play();
			$('#audiojs_wrapper_' + index + ' > .play-pause').addClass('playing');
			var min: any = Math.floor(wavesurfer.getDuration() / 60);
			var sec: any = Math.floor(wavesurfer.getDuration() - min * 60);
			if (min < 10) {
				min = '0' + min;
			}
			if (sec < 10) {
				sec = '0' + sec;
			}
			$('#total_time_' + index).html(min + ':' + sec);
		});

		this.element.nativeElement.querySelector("#play_button_" + index).style.display = "none";
		$("#play_pause_button_" + index).show();

		this.musicPlayer = wavesurfer;

		wavesurfer.on('audioprocess', function () {

			let min_tot: any = 0;
			let sec_tot: any = Math.floor(wavesurfer.getCurrentTime());

			if (wavesurfer.getCurrentTime() > 60) {
				min_tot = Math.floor(wavesurfer.getCurrentTime() / 60);
				sec_tot = Math.floor(wavesurfer.getCurrentTime() - min_tot * 60);
			}
			if (sec_tot < 10) {
				sec_tot = '0' + sec_tot;
			}
			if (min_tot < 10) {
				min_tot = '0' + min_tot;
			}
			$('#current_time_' + index).html(min_tot + ':' + sec_tot);

			let percnt_load = Math.ceil((wavesurfer.getCurrentTime() * 100) / wavesurfer.getDuration());
			//console.log(wavesurfer.getCurrentTime() +'/'+ wavesurfer.getDuration())
			$('#scrubber_' + index + ' > .progress').css('width', percnt_load + "%");
		});

		wavesurfer.on('finish', function () {
			$("#play_pause_button_" + index + " > i").removeClass('glyphicon-pause');
			$("#play_pause_button_" + index + " > i").addClass('glyphicon-play');
		});

  }
  
  boostpost(postid:any,loginuserid:any){
		let senddata = {"postid":postid,"loginuserid":loginuserid};
			this.commonService.boostpost(senddata)
			.subscribe(
			(data: any) => {
				console.log(data);
				if(data.success == 1){
					$("#boostsuccess"+postid).html(data.msg);
					$("#boostsuccess"+postid).addClass("showhide");
					setTimeout(function(){ $("#boostsuccess"+postid).removeClass("showhide"); }, 3000);
				}else{
					$("#boosterror"+postid).html(data.msg);
					$("#boosterror"+postid).addClass("showhide");
					setTimeout(function(){ $("#boosterror"+postid).removeClass("showhide"); }, 3000);
				}
				
			},
			error => {
				//this.alertService.error(error);
				//this.errorCheck = false;
			}
		);
  }
  
  getUserProfile(): void{
		let pf_data = { username: this.userInformation.user_info.username, current_user_id: this.userInformation.user_info.id }
		this.commonService.getProfileDetails(pf_data).subscribe(
			(data: any) => {
				console.log(data)
				if (data != false) {
					this.asyncdataService.setVotesCount(data.user_info.total_votes);
					this.asyncdataService.setAudioVotesCount(data.user_info.total_audio_votes);
					this.asyncdataService.setVideoVotesCount(data.user_info.total_video_votes);									
				}
			},
			error => { },
		);
  }
  
  deletemypost(post:any,loginuserid:any){
		const self = this;
		bootbox.confirm({
            message: 'Are you sure you want to delete this post?',
            buttons: {
                cancel: {
                    className: 'btn-cancel'
                }
            },
            callback: function (result: any) {
                if (result) {
                    if (post.id) {
                        const postid = post.id;
						let senddata = {"postid":postid,"loginuserid":loginuserid};
						self.commonService.deletepost(senddata).subscribe(
							(data: any) => {
								console.log(data);
								if(data.success == 1){
									self.getUserProfile();
									if( post.media_type === 'video'){
										const tmp = { type: 'remove', count: 1};
										self.asyncdataService.setTotalVideos(tmp);
									} else if( post.media_type === 'audio'){
										const tmp = { type: 'remove', count: 1};
										self.asyncdataService.setTotalAudios(tmp);
									} 

									$("#boostsuccess"+postid).html(data.msg);
									$("#boostsuccess"+postid).addClass("showhide");
									setTimeout(function(){ 
										$(".postdiv"+postid).slideUp("slow", function() {
											$(".postdiv"+postid).remove();
											$("#boostsuccess"+postid).removeClass("showhide");
										});
									}, 2000);
								}else{
									$("#boosterror"+postid).html(data.msg);
									$("#boosterror"+postid).addClass("showhide");
									setTimeout(function(){ $("#boosterror"+postid).removeClass("showhide"); }, 2000);
								}
							},
							error => {}
						);
                    }
                }
            }
        });
  }
  
  pauseMusic(index: any) {

		if ($('#audiojs_wrapper_' + index + ' > .play-pause').hasClass('playing')) {

			this.musicPlayer.playPause();

			if ($("#play_pause_button_" + index + " > i").hasClass('glyphicon-play')) {
				$("#play_pause_button_" + index + " > i").removeClass('glyphicon-play');
				$("#play_pause_button_" + index + " > i").addClass('glyphicon-pause');
			} else {
				$("#play_pause_button_" + index + " > i").removeClass('glyphicon-pause');
				$("#play_pause_button_" + index + " > i").addClass('glyphicon-play');
			}
		}
  }
  
  stopMusic(index: any) {

		if ($('#audiojs_wrapper_' + index + ' > .play-pause').hasClass('playing')) {

			this.musicPlayer.stop();
			$('#scrubber_' + index + ' > .progress').css('width', "0%");
			$('#current_time_' + index).html('00:00');
			$("#play_pause_button_" + index + " > i").removeClass('glyphicon-pause');
			$("#play_pause_button_" + index + " > i").addClass('glyphicon-play');
		}
  }
  
  toggleMute(index: any) {

		if ($('#audiojs_wrapper_' + index + ' > .play-pause').hasClass('playing')) {

			if (this.musicPlayer) {
				this.musicPlayer.toggleMute();
			}
			if ($('#volume-area_' + index + ' > .volume > i').hasClass('glyphicon-volume-up')) {
				$('#volume-area_' + index + ' > .volume > i').removeClass('glyphicon-volume-up');
				$('#volume-area_' + index + ' > .volume > i').addClass('glyphicon-volume-off');
			} else {

				$('#volume-area_' + index + ' > .volume > i').removeClass('glyphicon-volume-off');
				$('#volume-area_' + index + ' > .volume > i').addClass('glyphicon-volume-up');
			}
		}
  }
  
  forwardMusic(index: any) {

		if ($('#audiojs_wrapper_' + index + ' > .play-pause').hasClass('playing')) {
			if (this.musicPlayer) {
				this.musicPlayer.skipForward(5);
			}
		}
	}
	backwardMusic(index: any) {

		if ($('#audiojs_wrapper_' + index + ' > .play-pause').hasClass('playing')) {
			if (this.musicPlayer) {
				this.musicPlayer.skipBackward(5);
			}
		}
	}
  
  private _getTotalMedia(): void{
		let dataObj:any = { login_user_id:this.userInformation.user_info.id, username: this.userInformation.user_info.username};
		this.commonService.geTotalMedia(dataObj,this.userInformation.api_key).subscribe(
			(data:any) => {
				
				this.userInformation.user_info.total_videos = data.data.total_video;
				this.userInformation.user_info.total_audios = data.data.total_audio;
				localStorage.setItem('currentUser', JSON.stringify(this.userInformation));

				const tmp1 = { type: 'add', count: data.data.total_video};
				this.asyncdataService.setTotalVideos( tmp1);
				const tmp2 = { type: 'add', count: data.data.total_audio};
				this.asyncdataService.setTotalAudios( tmp2);
			},
			error => {}
		);
	}

}
