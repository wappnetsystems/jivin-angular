import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, NgModule, Input, AfterViewChecked, Renderer, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NgForm, FormControl, FormGroup } from '@angular/forms';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { DevConfig } from '../env.config';
declare var $: any;

@Component({
  selector: 'app-signupstep5',
  templateUrl: './signupstep5.component.html',
  styleUrls: ['./signupstep5.component.css']
})
export class Signupstep5Component implements OnInit, AfterViewChecked, OnDestroy {

  regF: NgForm;
  @ViewChild('regF', { static: true }) currentForm: NgForm;
  //test_var:any="test2";
  userInfo: any;
  items: any = [];
  regData: any = {};
  loading: boolean = false;
  errorCheck: boolean = false;
  errorMessage: string = "<strong>Opps!</strong><span>&nbsp;&nbsp;&nbsp;Error occured.Please try again..</span>";
  jivin_video_link: any = "";
  jivin_video_title: string = "";
  username_readonly: boolean = false;
  email_readyonly: boolean = true;
  api_response: any = {};
  formErrors: any = {
    'name': '', 'power': ''
  };

  validationMessages: any = {
  };

  constructor(
    private router: Router,
    private _location: Location, private renderer: Renderer, private http: Http) {
    this.renderer.setElementClass(document.body, 'register_bg', true);
    let step_calculator = localStorage.getItem('regForm.step');

    if (step_calculator != '5') {

      this._location.back();
    }
  }

  ngOnInit() {
    this.userInfo = JSON.parse(localStorage.getItem('regForm.u_data'));
    //console.log(JSON.parse(this.userInfo));
    //console.log(this.userInfo);
    this.regData.year = "";
    this.regData.month = "";
    localStorage.setItem('regForm.step', '6');
    this.regData.email = this.userInfo.email;
    let headers = new Headers();
    headers.append('Content-type', 'application/json');

    let options = new RequestOptions({ headers: headers });

    this.http.get(DevConfig.API + 'api/v1/get_jivin_video', options)
      .subscribe((data) => {
        this.api_response = data.json();
        this.jivin_video_link = this.api_response.data.video_link;

        //this.jivin_video_title=this.api_response.data.video_title;
      }, error => {
        alert("Error occurred during playing video of jivin introductory.");
      });
  }
  ngOnDestroy() {
    this.renderer.setElementClass(document.body, 'register_bg', false);
  }
  ngAfterViewChecked() {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.regF) { return; }
    this.regF = this.currentForm;
    if (this.regF) {
      this.regF.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }

  onValueChanged(data?: any) {
    if (!this.regF) { return; }
    const form = this.regF.form;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  createYearRange(number: any) {

    this.items = [];
    for (var i = 2007; i >= number; i--) {
      this.items.push(i);
    }
    return this.items;
  }

  formReg() {
    //console.log(this.regData);
    this.loading = true;
    if (this.regF.form.valid) {

      this.userInfo.username = this.regData.username;
      this.userInfo.password = this.regData.password;
      this.userInfo.birth_month = this.regData.month;
      this.userInfo.birth_year = this.regData.year;
      this.userInfo.finish_reg = 1;
      if (localStorage.getItem('user_name')) {
        this.userInfo.email = this.regData.email;
      }
      console.log(this.regData.email);
      //this.userInfo.user_id = this.userInfo.id;
      //this.userInfo.is_following = localStorage.getItem('follow');
      //console.log(typeof this.userInfo.user_id);
      this.http.post(DevConfig.API + 'api/v1/register', JSON.stringify(this.userInfo))
        .subscribe((data) => {
          this.api_response = data.json();
          if (this.api_response.success == 0) {

            this.errorCheck = true;
            if (this.api_response.msg) {
              this.errorMessage = "<strong>Opps!</strong><span>&nbsp;&nbsp;&nbsp;" + this.api_response.msg + "</span>";
            }
            this.loading = false;


          } else {
            this.errorCheck = false;
            localStorage.setItem('regForm.u_data', JSON.stringify(this.userInfo));
            //this.loading = false;
            this.router.navigate(['/signup6']);
          }
        }, error => {
          this.loading = false;
          alert('Error Occurred. Try Again!');

        })


    }

    setTimeout(() => {
      this.loading = false;
    }, 6000);
    //console.log(localStorage.getItem('regForm.u_data'));
  }

}
