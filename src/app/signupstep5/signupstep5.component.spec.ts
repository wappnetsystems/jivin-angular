import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signupstep5Component } from './signupstep5.component';

describe('Signupstep5Component', () => {
  let component: Signupstep5Component;
  let fixture: ComponentFixture<Signupstep5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Signupstep5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Signupstep5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
